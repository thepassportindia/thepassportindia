<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	Route::get('/passdashboard/', 'Pagecontroller@passdashboard');
	Route::get('/view/{id}', 'Pagecontroller@view')->name('view');
	Route::get('/passdashboard/newpassport', 'Pagecontroller@newpassport');
	Route::get('/passdashboard/reissue', 'Pagecontroller@reissue');
	Route::post('/search', 'Pagecontroller@search')->name('search');
	});





// Header Page Route

Route::get('/', function () { return view('passport');});
Route::get('/passport', function () { return view('passport');});
Route::get('/process', function () { return view('pages.process');});
Route::get('/fees', function () { return view('pages.fees');});
Route::get('/documents', function () { return view('pages.documents');});
Route::get('/faq', function () { return view('pages.faq');});
Route::get('/contact', function () { return view('pages.contact');});
Route::get('/makepayment', function () { return view('makepayment');});


// Footer Page Route
Route::get('/about', function () { return view('pages.about');});
Route::get('/terms', function () { return view('pages.terms');});
Route::get('/polices', function () { return view('pages.polices');});
Route::get('/disclaimer', function () { return view('pages.disclaimer');});
Route::get('/refund', function () { return view('pages.refund');});

Route::post('/store', 'Datacontroller@store')->name('store');
Route::get('/onlinepayment/{uniq_id}', 'Pagecontroller@onlinepayment')->name('onlinepayment');





Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);
