/*COMMON PAGE*/
/*============*/

/*dynamic padding top*/
$(document).ready(function() {
    var divHeight = $('#header').height();
    $('#main-content').css('padding-top', divHeight+'px');

        var divHeightsmall = $('#header_small').height();
    $('#main-content').css('padding-top', divHeight+'px');
});


/* Retrive districts */
$('#applicant-state').change(function (){
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_districts.php?state_id=' + itemCatID, function (items){
      $("#applicant-district").empty();
      $("#applicant-district").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#applicant-district").append("<option value='" + item.state_id + "'>"  + item.district_name + "</option>" );
      });
    });
  }
});


$('#postal-state').change(function (){
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_districts.php?state_id=' + itemCatID, function (items){
      $("#sel_adr_res_Pstation").empty();
      $("#sel_adr_res_Pstation").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#sel_adr_res_Pstation").append("<option value='" + item.district_id+'-'+item.state_id + "'>"  + item.district_name + "</option>" );
      });
    });
  }
});

$('#txt_adr_per_state').change(function (){
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_districts.php?state_id=' + itemCatID, function (items){
      $("#txt_adr_per_district").empty();
      $("#txt_adr_per_district").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#txt_adr_per_district").append("<option value='" + item.district_id+'-'+item.state_id + "'>"  + item.district_name + "</option>" );
      });
    });
  }
});




$('#postal-state').change(function (){
  //alert('hii');
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_districts.php?state_id=' + itemCatID, function (items){
      $("#postal-district").empty();
      $("#postal-district").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#postal-district").append("<option value='" + item.district_id+'-'+item.state_id + "'>"  + item.district_name + "</option>" );
      });
    });
  }
});

$('#postal-state-table').change(function (){
  //alert('hii');
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_districts.php?state_id=' + itemCatID, function (items){
      $("#postal-district-table").empty();
       $("#sel_adr_res_Pstation_table").empty();
      $("#postal-district-table").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#postal-district-table").append("<option value='" + item.district_id+'-'+item.state_id + "'>"  + item.district_name + "</option>" );
      });
    });
  }
});


$('#txt_adr_per_district').change(function (){
  var itemCatID = $(this).val();
  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_police_station.php?district_id=' + itemCatID, function (items){
      $("#sel_adr_per_Pstation").empty();
      $("#sel_adr_per_Pstation").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#sel_adr_per_Pstation").append("<option value='" + item.police_station_id + "'>"  + item.police_station_name + "</option>" );
      });
    });
  }
});




/* Retrive police station */
$('#postal-district').change(function (){
  var itemCatID = $(this).val();

  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_police_station.php?district_id=' + itemCatID, function (items){
      $("#sel_adr_res_Pstation").empty();
      $("#sel_adr_res_Pstation").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#sel_adr_res_Pstation").append("<option value='" + item.police_station_id + "'>"  + item.police_station_name + "</option>" );
      });
    });
  }
});


/* Retrive police station */
$('#postal-district-table').change(function (){
  var itemCatID = $(this).val();

  if (itemCatID)
  {
    $.getJSON('assets/php/retrive_police_station.php?district_id=' + itemCatID, function (items){
      $("#sel_adr_res_Pstation_table").empty();
      var i=1;
      //$("#sel_adr_res_Pstation").append("<option selected='selected' value=''>Please Select</option>" );
      $.each(items,function (id,item){
          $("#sel_adr_res_Pstation_table").append("<tr><td>"+ i +"</td><td>"  + item.police_station_name + "</td><td>"+item.district_id+"</td></tr>" );
      i++;
      });
    });
  }
});

$('.datepicker').datepicker({
  format: 'dd-mm-yyyy',
  autoclose: true
});

/*noback*/
function noBack() { window.history.forward(); }

/*dynamic padding top*/
$(document).ready(function() {
    var divHeight = $('#navbar-header').height();
    $('#main-content').css('padding-top', divHeight+'px');

     var divHeight = $('#navbar-header-small').height();
    $('#main-content').css('padding-top', divHeight+'px');
});


/*window.onscroll = function() {myFunction()};

var header = document.getElementById("header");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}*/


/*MSME PAGE*/
/*===================*/
$('#major-activity-of-unit').change(function (){
  var value = $(this).val();
  if (value == 'manufacturer'){
    $("#nic-2-digit-code-manufacturer").removeClass('dn');
    $("#nic-2-digit-code-manufacturer").attr("required", true);
    $("#nic-2-digit-code-service-provider").attr("required", false);
    $("#nic-2-digit-code-service-provider").addClass('dn');
    $("#2-digit-code0").addClass('dn');
    $("#nic-2-digit-code-manufacturer-selected").addClass('dn');
    $("#nic-2-digit-code-service-provider").attr("disabled", true);
    $("#nic-2-digit-code-manufacturer").attr("disabled", false);
    $("#nic-2-digit-code-manufacturer-selected").attr("disabled", true);
  }
  if (value == 'service_provider'){
    $("#nic-2-digit-code-manufacturer").addClass('dn');
    $("#nic-2-digit-code-service-provider").removeClass('dn');
    $("#nic-2-digit-code-manufacturer").attr("required", false);
    $("#nic-2-digit-code-service-provider").attr("required", true);
    $("#2-digit-code0").addClass('dn');
    $("#nic-2-digit-code-manufacturer-selected").addClass('dn');
    $("#nic-2-digit-code-service-provider").attr("disabled", false);
    $("#nic-2-digit-code-manufacturer").attr("disabled", true);
    $("#nic-2-digit-code-manufacturer-selected").attr("disabled", true);
  }
});

$('#same-as-above').change(function (){
  $('#office-address').attr('disabled', this.checked);
  $('#postal-state').attr('disabled', this.checked);
  $('#postal-district').attr('disabled', this.checked);
  $('#office-pin').attr('disabled', this.checked);
});
/*razorpay payment*/
$(document).on("click", "#msme-razor-pay", function (e) {

    var email = $(this).data('email-id');
    var name = $(this).data('name');

    var options = {
        "key": "rzp_live_NbF2epK48Z0czP",
        //"amount": "100", // 2000 paise = INR 20
        "amount": "175000", // 2000 paise = INR 20
        "name": "OnlineMSME",
        "description": "Purchase Description",
        "image": "assets/img/favicon.ico",
        "handler": function (response){
            //alert(response.razorpay_payment_id);
            document.getElementById("rzorpay-transaction-id").value = response.razorpay_payment_id;
            document.getElementById("msme-razorpay").submit()
        },
        "prefill": {
            "name": ""+name+"",
            "email": ""+email+""
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#528FF0"
        }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();
    e.preventDefault();
});
/* //razorpay form*/

/*enquiry form*/
$("#passport-enquiry-form").submit(function(e) {
  e.preventDefault();
  $("#submit-btn").addClass('dn');
  $("#submit-loader").removeClass('dn');
   var act = document.getElementById("enq_type").value;

  $.ajax({
    type: "POST",
    url: "assets/php/ajax_functions.php",
    data: $(this).serialize()+"&action="+act,
    dataType: "json",
    success: function(data){
      if(data.status == 1) {
        //success message
        $("#submit-loader").addClass('dn');
        $("#passport-enquiry-form").addClass('dn');
        $("#passport-enquiry-form-thank-you").removeClass('dn');
      }
    }
  });
});

/*enquiry form*/
$("#passport-enquiry-form1").submit(function(e) {
  e.preventDefault();
  $("#submit-btn1").addClass('dn');
  $("#submit-loader1").removeClass('dn');
   var act = document.getElementById("enq_type").value;

  $.ajax({
    type: "POST",
    url: "assets/php/ajax_functions.php",
    data: $(this).serialize()+"&action="+act,
    dataType: "json",
    success: function(data){
      if(data.status == 1) {
        //success message
        $("#submit-loader1").addClass('dn');
        $("#passport-enquiry-form1").addClass('dn');
        $("#passport-enquiry-form-thank-you1").removeClass('dn');
      }
    }
  });
});

$('#frmApp').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });

/*enquiry form*/
$("#reissue-passport-enquiry-form").submit(function(e) {
  e.preventDefault();
  $("#submit-btn").addClass('dn');
  $("#submit-loader").removeClass('dn');
  $.ajax({
    type: "POST",
    url: "assets/php/ajax_functions.php",
    data: $(this).serialize()+"&action=reissue-passport-enquiry-form",
    dataType: "json",
    success: function(data){
      if(data.status == 1) {
        //success message
        $("#submit-loader").addClass('dn');
        $("#reissue-passport-enquiry-form").addClass('dn');
        $("#reissue-passport-enquiry-form-thank-you").removeClass('dn');
      }
    }
  });
});

window.onscroll = function() {myFunction()};

var header = document.getElementById("header");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

