<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formdata extends Model
{
    public $timestamps = false;
    protected $table = 'passportdata'; 
    
    protected $fillable = [
        'applying', 
        'type_of_application',
        'type_of_booklet',

        'first_name', 
        'middle_name',
        'surname',
        'aadhaar_no',
        'gender',
        'marital_status',
        'date_of_birth',
        'birth_out_of_india',
        'village_or_town_or_city', 
        'country',
        'state',
        'district',
        'citizenship_of_india_by',
        'pan',
        'voter_id',
        'educational',
        'employment',
        'organisation',
        'is_your_parent',
        'non_ecr_category',
        'body_mark',
        'other_name',
        'alias_f',
        'alias_m',
        'alias_l',
        'change_name',

        'father_f_name',
        'father_l_name',
        'father_m_name',
        'mother_f_name',
        'mother_l_name',
        'mother_m_name',
        'gard_f_name',
        'gard_l_name',
        'gard_m_name',
        'spz_f_name',
        'spz_l_name',
        'spz_m_name',

        'present_address_type',
        'house_no',
        'town',
        'country_4',
        'state_4',
        'district_4',
        'pincode',
        'police_station',
        'mobile',
        'email',
        'permanent_address',
        'permanent_address_type',
        'p_house_no',
        'p_town',
        'p_country',
        'p_state',
        'p_district',
        'p_pincode',
        'p_police_station',

        'name_and_address',
        'mobile_no',
        'email_id',

        'hold_any_Identity',
        'passport_number',
        'date_of_issue',
        'date_of_expiry',
        'place_of_issue',
        'file_number',
        'detail_of_prev_pass',
        'but_not_issued',
        'file_number_not_iss',
        'month_and_year_applying',
        'pass_office_where_applied',

        'address_proof_7',
        'd_o_b_proof_7',

    ];
}

