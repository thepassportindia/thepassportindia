<?php

namespace App\Http\Controllers;

use App\Formdata;
use Illuminate\Http\Request;



class Datacontroller extends Controller
{


    public function store(Request $request)
    {
        //dd($request);
        $insertdata = new Formdata([
            'applying' => $request->get('1_application_for'),
            'type_of_application' => $request->get('sel_type_appl'),
            'type_of_booklet' => $request->get('1_type_of_booklet'),

            'first_name' => $request->get('2_fname'),
            'middle_name' => $request->get('2_mname'),
            'surname' => $request->get('2_sname'),
            'aadhaar_no' => $request->get('2_aadhar_no'),
            'gender' => $request->get('2_gender'),
            'marital_status' => $request->get('2_marital_status'),
            'date_of_birth' => $request->get('2_dob'),
            'birth_out_of_india' => $request->get('2_place_of_birth'),
            'village_or_town_or_city' => $request->get('2_village'),
            'country' => $request->get('2_country'),
            'state' => $request->get('2_state'),
            'district' => $request->get('2_district'),
            'citizenship_of_india_by' => $request->get('2_citizenship'),
            'pan' => $request->get('2_pan'),
            'voter_id' => $request->get('2_voter'),
            'educational' => $request->get('2_qualification'),
            'employment' => $request->get('2_employment_type'),
            'organisation' => $request->get('2_organization_name'),
            'is_your_parent' => $request->get('2_sapouse'),
            'non_ecr_category' => $request->get('2_non_ecr'),
            'body_mark' => $request->get('2_body_mark'),
            'other_name' => $request->get('2_alias_type'),
            'alias_f' => $request->get('2_alias_fname'),
            'alias_m' => $request->get('2_alias_mname'),
            'alias_l' => $request->get('2_alias_lname'),
            'change_name' => $request->get('2_previous_type'),

            'father_f_name' => $request->get('3_father_f_name'),
            'father_m_name' => $request->get('3_father_l_name'),
            'father_l_name' => $request->get('3_father_m_name'),
            'mother_f_name' => $request->get('3_mother_f_name'),
            'mother_l_name' => $request->get('3_mother_l_name'),
            'mother_m_name' => $request->get('3_mother_m_name'),
            'gard_f_name' => $request->get('3_gard_f_name'),
            'gard_l_name' => $request->get('3_gard_l_name'),
            'gard_m_name' => $request->get('3_gard_m_name'),
            'spz_f_name' => $request->get('3_spz_f_name'),
            'spz_l_name' => $request->get('3_spz_l_name'),
            'spz_m_name' => $request->get('3_spz_m_name'),

            'present_address_type' => $request->get('4_present_address_type'),
            'house_no' => $request->get('4_house_no'),
            'town' => $request->get('4_town'),
            'country_4' => $request->get('4_country'),
            'state_4' => $request->get('4_state'),
            'district_4' => $request->get('4_district'),
            'pincode' => $request->get('4_pincode'),
            'police_station' => $request->get('4_police_station'),
            'mobile' => $request->get('4_mobile'),
            'email' => $request->get('4_email'),
            'permanent_address' => $request->get('4_permanent_address'),
            'permanent_address_type' => $request->get('4_permanent_address_type'),
            'p_house_no' => $request->get('4_p_house_no'),
            'p_town' => $request->get('4_p_town'),
            'p_country' => $request->get('4_p_country'),
            'p_state' => $request->get('4_p_state'),
            'p_district' => $request->get('4_p_district'),
            'p_pincode' => $request->get('4_p_pincode'),
            'p_police_station' => $request->get('4_p_police_station'),


            'name_and_address' => $request->get('5_name_address'),
            'mobile_no' => $request->get('5_mobile'),
            'email_id' => $request->get('5_email'),

            'hold_any_Identity' => $request->get('6_id_certify_type'),
            'passport_number' => $request->get('6_passport_no'),
            'date_of_issue' => $request->get('6_date_of_issue'),
            'date_of_expiry' => $request->get('6_date_of_expiry'),
            'place_of_issue' => $request->get('6_place_issue'),
            'file_number' => $request->get('6_file_no'),
            'detail_of_prev_pass' => $request->get('6_diplomatic_passport'),
            'but_not_issued' => $request->get('6_applied_status'),
            'file_number_not_iss' => $request->get('6_applied_file_no'),
            'month_and_year_applying' => $request->get('6_applying'),
            'pass_office_where_applied' => $request->get('6_applied_office'),

            'address_proof_7' => $request->get('8_document_proof'),
            'd_o_b_proof_7' => $request->get('8_dob_proof'),


        ]);
        $insertdata->uniq_id ='PASS' . rand(1,1000000);
        $insertdata->save();

        return redirect('/onlinepayment/'.$insertdata->uniq_id);
    }



}

