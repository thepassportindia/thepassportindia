<?php

namespace App\Http\Controllers;

use App\Formdata;
use Illuminate\Http\Request;

use DB;

class Pagecontroller extends Controller
{



    public function chirag()
    {
        $data = Formdata::paginate(10);

        return view('output', compact('data'));
    }

    public function passdashboard()
    {
        $data = Formdata::paginate(15);

        return view('passdashboard', compact('data'));
    }


    public function newpassport()
    {
        $data = Formdata::paginate(5);

        return view('newpassport', compact('data'));
    }

    public function reissue()
    {
        $data = Formdata::paginate(5);

        return view('reissue', compact('data'));
    }


    public function view($id)
    {
        $info = Formdata::find($id);

        return view('view', compact('info'));  
    }

    public function search(Request $request)
    {
        $search = $request->get('name');
        $stores = Formdata::where('email', 'LIKE', "%$search%")
                            ->orWhere('mobile', 'LIKE', "%$search%")
                            ->orWhere('mobile_no', 'LIKE', "%$search%")
                            ->orWhere('email_id', 'LIKE', "%$search%")
                            ->orWhere('first_name', 'LIKE', "%$search%")
                            ->orWhere('uniq_id', 'LIKE', "%$search%")
                            ->get();
        
        return view ('search', compact('stores'));
        
    }

    public function onlinepayment($uniq_id)
    {


        $info = DB::table('passportdata')
                    ->where('passportdata.uniq_id', $uniq_id)                  
                    ->select('passportdata.*')                                       
                    ->first();

        return view('pages.onlinepayment', compact('info'));  
    }



}

