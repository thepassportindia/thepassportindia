<!DOCTYPE html>
<html lang="en">
<head>
    

@include('includes.head')


    <script src="js/jquery.min.js"></script>
    <script src="js/validate.js"></script>

<!--     <script type="text/javascript">
        $(document).ready(function() {

            $('#contact_2_state').on('change', function() {
                var stateID = $(this).val();

                //alert (qualificationID);

                if (stateID) {
                    $.ajax({
                        type: 'POST',
                        url: 'locationData.php',

                        data: 'state_id=' + stateID,
                        success: function(html) {
                            $('#contact_2_district').html(html);

                        }
                    });
                } else {
                    $('#contact_2_district').html('<option value="">Select state first</option>');

                }
            });

        });

        //End script
    </script> -->
    <style>
        h1 {
            text-align: center;
            color: #000;
            "

        }
        
        h5 {
            margin-top: 30px;
            text-align: center;
        }
        
        marquee {
            background-color: #5DA6D3;
            font-size: 16px;
            padding-top: 5px;
            padding-bottom: 5px;
            color: #000;
            font-weight: 700;
            letter-spacing: 3px;
        }
        
        .img-11 {
            display: none;
        }
        
        .img-21 {
            display: none;
        }
        
        .img-2>h3 {
            text-align: center;
            font-size: 30px;
            margin-bottom: -20px;
        }
        
        .section-head {
            background-color: #5DA6D3;
            width: 100%;
            #border-radius: 10px;
            margin-bottom: 15px;
        }
        
        .card {
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            #border: 2px solid #686868;
            #border-radius: 15px;
            -webkit-box-shadow: 10px 10px 15px 2px #686868;
        }
        
        h2 {
            color: #1dc8cd;
        }
        
        h4 {
            color: white;
        }
        
        .enq {
            padding-left: 80px;
            color: #ffffff;
            font-size: 20px;
        }
        
        .enq-bg {
            #background: #ff8923;
            background: #2e5fa7;
            padding: 10px 10px 10px 10px;
        }
        
        .marq-blink {
            #animation: blinkingText 1s infinite;
        }
        
        @keyframes blinkingText {
            0% {
                color: #000;
            }
            #49% {
                color: transparent;
            }
            #50% {
                color: transparent;
            }
            #99% {
                color: transparent;
            }
            100% {
                color: #f28727;
            }
        }
        
        p {
            color: black;
        }
        
        .error {
            color: red;
        }
        
        .navbar-default {
            margin-top: 140px;
        }
        
        .top-logo {
            background-color: #ffffff;
            #margin-left: 20px;
            padding-left: 15px;
            margin-right: -10px;
            background: cover;
        }
        
        .faq-head {
            background: gray;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .btn-warning {
            background: #3e70cb;
            font-size: 16px;
            font-weight: 600;
            padding: 15px 25px 15px 25px;
        }
        
        @media only screen and (max-width: 600px) {
            .h4,
            .h5,
            .h6,
            h4,
            h5,
            h6 {
                margin-top: 0px;
                margin-bottom: 10px;
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .main-header {
                min-height: 110px;
            }
            .img-1 {
                display: none;
                height: 10px;
                width: 10px;
            }
            .img-2 {
                display: none;
                #font: 10px;
                #text-align: right;
                #padding-left: 10px;
            }
            .img-21 {
                display: block;
            }
            .img-21>h3 {
                font-size: 16px;
                text-align: left;
            }
            .img-2 h3 {
                font-size: 20px;
                text-align: right;
                margin-top: 10px;
            }
            .img-3 {
                display: none;
            }
            .img-11 {
                display: none;
                #height: 15px;
                #width: 15px;
                #margin-left: 5px;
            }
            .top-nav-collapse {
                #padding: 25px 0;
            }
            .navbar-default {
                margin-top: 0px;
            }
            .form-control {
                margin-top: 10px;
            }
            h1 {
                font-size: 25px;
            }
        }
    </style>

    <style>
        .error {
            display: none;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid {
            border: 2px solid green;
        }
    </style>

</head>

<body>

    <!--header-->
    @include('includes.header1')

    <!--/ header-->
    <marquee scrollamount="15">Please note that this website is owned by a private organisation & Not associated with Ministry of External Affairs (MEA)</marquee>

    <link rel="stylesheet" href="../code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="../code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
    <script src="../code.jquery.com/jquery-1.12.4.js"></script>
    <script src="../code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="newapply.js"></script>

    <!---->

    <style>
        .error {
            display: none;
            //margin-left: 10px;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid,
        select.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid,
        select.valid {
            border: 2px solid green;
        }
    </style>
    <script>
        $("#myform").validate({
            ignore: ".ignore"
        });
    </script>

<form method="post" action="{{ route('store') }}">
{{ csrf_field() }}
        <h1>New/ Fresh Passport</h1>
        <hr size="4px">

        <section class="section-padding ">
            <div class="container card">
                <div class="row white">
                    <div class="col-md-8 col-sm-12 section-head">
                        <div class="section-title">
                            <h4>1. Service Required</h4>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">

                            <div class="col-md-4 top-10">
                                <label>Applying for
                                    <!--<span style="color:red">*</span>--></label>
                                <div class="form-holder">
                                    <select class="form-control frmApp" name="1_application_for" id="1_application_for">
                                        <option value="New / Fresh Passport" selected>New / Fresh Passport</option>
                                        <option value="Reissue Passport" >Reissue Passport</option>
                                    </select>

                                </div>

                                <div class="clb"></div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Type of Application <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <select class="form-control " name="sel_type_appl" id="sel_type_appl" onchange="flip_taatkal_note();">
                                        <option value="Normal" selected>Normal</option>
                                        <option value="Tatkaal">Tatkaal</option>
                                    </select>
                                    <div id="divCheckPasswordMatch9"></div>
                                </div>

                                <div class="clb"></div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Type of Passport Booklet <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <select class="form-control " name="1_type_of_booklet" id="contact_1_type_of_booklet">
                                        <option value="36 Pages" selected>36 Pages</option>
                                        <option value="60 Pages">60 Pages</option>
                                    </select>
                                    <div id="divCheckPasswordMatch10"></div>
                                </div>
                            </div>

                            <div id="tatkal_note" style="display:none;">
                                <div class="col-md-12 top-10">
                                    <p class="marq-blink text-center" style="font-size: 15px;"><b>Note: For Tatkal Scheme, you need to pay Additional Tatkal Fees of Rs. 2000/- at Passport Seva Kendra during your Appointment.</b></p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>

        <!---------------------------------Section Second-------------------------------------------->

<br>        
        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>2. Applicant Details</h4>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">

                    <div class="row">

                        <div class="col-md-4">
                            <label>Applicant's First Name <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control fname_2" placeholder="Applicant First Name" id="contact_2_fname" name="2_fname" maxlength="50" required="">
                                <span class="error" id="mylocation1"></span>
                                <span id="sp" style="color:red"></span>
                                <!--div id="sp" style="color:red"></div-->
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Middle Name" id="txt_middlename" name="2_mname" maxlength="50">
                                <span id="error_middle_name" style="color:red"></span>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Surname</label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Surname Name" id="txt_lastname" name="2_sname" maxlength="50">
                                <span id="error_last_name" style="color:red"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Aadhaar No</label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Aadhaar No. (12 Digit)" id="" name="2_aadhar_no" maxlength="12">
                                <span id="error_aadhar" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Gender <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <select class="form-control" name="2_gender" id="contact_2_gender" required="">
                                    <option value="" selected disabled>Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Transgender">Transgender</option>
                                </select>
                                <div id="error_gender" style="color:red"></div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Marital Status <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <select class="form-control select" name="2_marital_status" id="contact_2_marital_status" onchange="marital_chg_labels();" required="">
                                    <option value="" selected disabled>Select Marital Status</option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    <option value="Divorced">Divorced</option>
                                    <option value="Widow/Widower">Widow/ Widower</option>
                                </select>
                                <div id="marital" style="color:red"></div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Date of Birth <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="date" class="form-control date" placeholder="YYYY-MM-DD" id="contact_2_dob" name="2_dob" required="">
                                <div id="dob2" style="color:red"></div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Is your Place of Birth out of India? <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_place_birth" name="2_place_of_birth" onchange="flip_palce();" >

                                    <option value="No" selected>No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span class="error" id="mylocation63"></span>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>Village or Town or City <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Village or Town or City " name="2_village" id="contact_2_village" required="">
                                <span class="error" id="mylocation5">This field is required</span>
                                <div id="village" style="color:red"></div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Country <span style="color:red">*</span></label>
                            <div class="form-holder" id="othe_con" style="display: none;">
                                <select class="form-control" id="country" name="2_country">
                                    <option value="" disabled selected> Select Country</option>
                                    <option value='Afghanistan'>Afghanistan</option>
                                    <option value='Albania'>Albania</option>
                                    <option value='Algeria'>Algeria</option>
                                    <option value='American Samoa'>American Samoa</option>
                                    <option value='Andorra'>Andorra</option>
                                    <option value='Angola'>Angola</option>
                                    <option value='Anguilla'>Anguilla</option>
                                    <option value='Antarctica'>Antarctica</option>
                                    <option value='Antigua And Barbuda'>Antigua And Barbuda</option>
                                    <option value='Argentina'>Argentina</option>
                                    <option value='Armenia'>Armenia</option>
                                    <option value='Aruba'>Aruba</option>
                                    <option value='Australia'>Australia</option>
                                    <option value='Austria'>Austria</option>
                                    <option value='Azerbaijan'>Azerbaijan</option>
                                    <option value='Bahamas The'>Bahamas The</option>
                                    <option value='Bahrain'>Bahrain</option>
                                    <option value='Bangladesh'>Bangladesh</option>
                                    <option value='Barbados'>Barbados</option>
                                    <option value='Belarus'>Belarus</option>
                                    <option value='Belgium'>Belgium</option>
                                    <option value='Belize'>Belize</option>
                                    <option value='Benin'>Benin</option>
                                    <option value='Bermuda'>Bermuda</option>
                                    <option value='Bhutan'>Bhutan</option>
                                    <option value='Bolivia'>Bolivia</option>
                                    <option value='Bosnia and Herzegovina'>Bosnia and Herzegovina</option>
                                    <option value='Botswana'>Botswana</option>
                                    <option value='Bouvet Island'>Bouvet Island</option>
                                    <option value='Brazil'>Brazil</option>
                                    <option value='British Indian Ocean Territory'>British Indian Ocean Territory</option>
                                    <option value='Brunei'>Brunei</option>
                                    <option value='Bulgaria'>Bulgaria</option>
                                    <option value='Burkina Faso'>Burkina Faso</option>
                                    <option value='Burundi'>Burundi</option>
                                    <option value='Cambodia'>Cambodia</option>
                                    <option value='Cameroon'>Cameroon</option>
                                    <option value='Canada'>Canada</option>
                                    <option value='Cape Verde'>Cape Verde</option>
                                    <option value='Cayman Islands'>Cayman Islands</option>
                                    <option value='Central African Republic'>Central African Republic</option>
                                    <option value='Chad'>Chad</option>
                                    <option value='Chile'>Chile</option>
                                    <option value='China'>China</option>
                                    <option value='Christmas Island'>Christmas Island</option>
                                    <option value='Cocos (Keeling) Islands'>Cocos (Keeling) Islands</option>
                                    <option value='Colombia'>Colombia</option>
                                    <option value='Comoros'>Comoros</option>
                                    <option value='Republic Of The Congo'>Republic Of The Congo</option>
                                    <option value='Democratic Republic Of The Congo'>Democratic Republic Of The Congo</option>
                                    <option value='Cook Islands'>Cook Islands</option>
                                    <option value='Costa Rica'>Costa Rica</option>
                                    <option value='Cote D' Ivoire (Ivory Coast) '>Cote D'Ivoire (Ivory Coast)</option>
                                        <option value='Croatia (Hrvatska)'>Croatia (Hrvatska)</option>
                                        <option value='Cuba'>Cuba</option>
                                        <option value='Cyprus'>Cyprus</option>
                                        <option value='Czech Republic'>Czech Republic</option>
                                        <option value='Denmark'>Denmark</option>
                                        <option value='Djibouti'>Djibouti</option>
                                        <option value='Dominica'>Dominica</option>
                                        <option value='Dominican Republic'>Dominican Republic</option>
                                        <option value='East Timor'>East Timor</option>
                                        <option value='Ecuador'>Ecuador</option>
                                        <option value='Egypt'>Egypt</option>
                                        <option value='El Salvador'>El Salvador</option>
                                        <option value='Equatorial Guinea'>Equatorial Guinea</option>
                                        <option value='Eritrea'>Eritrea</option>
                                        <option value='Estonia'>Estonia</option>
                                        <option value='Ethiopia'>Ethiopia</option>
                                        <option value='External Territories of Australia'>External Territories of Australia</option>
                                        <option value='Falkland Islands'>Falkland Islands</option>
                                        <option value='Faroe Islands'>Faroe Islands</option>
                                        <option value='Fiji Islands'>Fiji Islands</option>
                                        <option value='Finland'>Finland</option>
                                        <option value='France'>France</option>
                                        <option value='French Guiana'>French Guiana</option>
                                        <option value='French Polynesia'>French Polynesia</option>
                                        <option value='French Southern Territories'>French Southern Territories</option>
                                        <option value='Gabon'>Gabon</option>
                                        <option value='Gambia The'>Gambia The</option>
                                        <option value='Georgia'>Georgia</option>
                                        <option value='Germany'>Germany</option>
                                        <option value='Ghana'>Ghana</option>
                                        <option value='Gibraltar'>Gibraltar</option>
                                        <option value='Greece'>Greece</option>
                                        <option value='Greenland'>Greenland</option>
                                        <option value='Grenada'>Grenada</option>
                                        <option value='Guadeloupe'>Guadeloupe</option>
                                        <option value='Guam'>Guam</option>
                                        <option value='Guatemala'>Guatemala</option>
                                        <option value='Guernsey and Alderney'>Guernsey and Alderney</option>
                                        <option value='Guinea'>Guinea</option>
                                        <option value='Guinea-Bissau'>Guinea-Bissau</option>
                                        <option value='Guyana'>Guyana</option>
                                        <option value='Haiti'>Haiti</option>
                                        <option value='Heard and McDonald Islands'>Heard and McDonald Islands</option>
                                        <option value='Honduras'>Honduras</option>
                                        <option value='Hong Kong S.A.R.'>Hong Kong S.A.R.</option>
                                        <option value='Hungary'>Hungary</option>
                                        <option value='Iceland'>Iceland</option>
                                        <option value='Indonesia'>Indonesia</option>
                                        <option value='Iran'>Iran</option>
                                        <option value='Iraq'>Iraq</option>
                                        <option value='Ireland'>Ireland</option>
                                        <option value='Israel'>Israel</option>
                                        <option value='Italy'>Italy</option>
                                        <option value='Jamaica'>Jamaica</option>
                                        <option value='Japan'>Japan</option>
                                        <option value='Jersey'>Jersey</option>
                                        <option value='Jordan'>Jordan</option>
                                        <option value='Kazakhstan'>Kazakhstan</option>
                                        <option value='Kenya'>Kenya</option>
                                        <option value='Kiribati'>Kiribati</option>
                                        <option value='Korea North'>Korea North</option>
                                        <option value='Korea South'>Korea South</option>
                                        <option value='Kuwait'>Kuwait</option>
                                        <option value='Kyrgyzstan'>Kyrgyzstan</option>
                                        <option value='Laos'>Laos</option>
                                        <option value='Latvia'>Latvia</option>
                                        <option value='Lebanon'>Lebanon</option>
                                        <option value='Lesotho'>Lesotho</option>
                                        <option value='Liberia'>Liberia</option>
                                        <option value='Libya'>Libya</option>
                                        <option value='Liechtenstein'>Liechtenstein</option>
                                        <option value='Lithuania'>Lithuania</option>
                                        <option value='Luxembourg'>Luxembourg</option>
                                        <option value='Macau S.A.R.'>Macau S.A.R.</option>
                                        <option value='Macedonia'>Macedonia</option>
                                        <option value='Madagascar'>Madagascar</option>
                                        <option value='Malawi'>Malawi</option>
                                        <option value='Malaysia'>Malaysia</option>
                                        <option value='Maldives'>Maldives</option>
                                        <option value='Mali'>Mali</option>
                                        <option value='Malta'>Malta</option>
                                        <option value='Man (Isle of)'>Man (Isle of)</option>
                                        <option value='Marshall Islands'>Marshall Islands</option>
                                        <option value='Martinique'>Martinique</option>
                                        <option value='Mauritania'>Mauritania</option>
                                        <option value='Mauritius'>Mauritius</option>
                                        <option value='Mayotte'>Mayotte</option>
                                        <option value='Mexico'>Mexico</option>
                                        <option value='Micronesia'>Micronesia</option>
                                        <option value='Moldova'>Moldova</option>
                                        <option value='Monaco'>Monaco</option>
                                        <option value='Mongolia'>Mongolia</option>
                                        <option value='Montserrat'>Montserrat</option>
                                        <option value='Morocco'>Morocco</option>
                                        <option value='Mozambique'>Mozambique</option>
                                        <option value='Myanmar'>Myanmar</option>
                                        <option value='Namibia'>Namibia</option>
                                        <option value='Nauru'>Nauru</option>
                                        <option value='Nepal'>Nepal</option>
                                        <option value='Netherlands Antilles'>Netherlands Antilles</option>
                                        <option value='Netherlands The'>Netherlands The</option>
                                        <option value='New Caledonia'>New Caledonia</option>
                                        <option value='New Zealand'>New Zealand</option>
                                        <option value='Nicaragua'>Nicaragua</option>
                                        <option value='Niger'>Niger</option>
                                        <option value='Nigeria'>Nigeria</option>
                                        <option value='Niue'>Niue</option>
                                        <option value='Norfolk Island'>Norfolk Island</option>
                                        <option value='Northern Mariana Islands'>Northern Mariana Islands</option>
                                        <option value='Norway'>Norway</option>
                                        <option value='Oman'>Oman</option>
                                        <option value='Pakistan'>Pakistan</option>
                                        <option value='Palau'>Palau</option>
                                        <option value='Palestinian Territory Occupied'>Palestinian Territory Occupied</option>
                                        <option value='Panama'>Panama</option>
                                        <option value='Papua new Guinea'>Papua new Guinea</option>
                                        <option value='Paraguay'>Paraguay</option>
                                        <option value='Peru'>Peru</option>
                                        <option value='Philippines'>Philippines</option>
                                        <option value='Pitcairn Island'>Pitcairn Island</option>
                                        <option value='Poland'>Poland</option>
                                        <option value='Portugal'>Portugal</option>
                                        <option value='Puerto Rico'>Puerto Rico</option>
                                        <option value='Qatar'>Qatar</option>
                                        <option value='Reunion'>Reunion</option>
                                        <option value='Romania'>Romania</option>
                                        <option value='Russia'>Russia</option>
                                        <option value='Rwanda'>Rwanda</option>
                                        <option value='Saint Helena'>Saint Helena</option>
                                        <option value='Saint Kitts And Nevis'>Saint Kitts And Nevis</option>
                                        <option value='Saint Lucia'>Saint Lucia</option>
                                        <option value='Saint Pierre and Miquelon'>Saint Pierre and Miquelon</option>
                                        <option value='Saint Vincent And The Grenadines'>Saint Vincent And The Grenadines</option>
                                        <option value='Samoa'>Samoa</option>
                                        <option value='San Marino'>San Marino</option>
                                        <option value='Sao Tome and Principe'>Sao Tome and Principe</option>
                                        <option value='Saudi Arabia'>Saudi Arabia</option>
                                        <option value='Senegal'>Senegal</option>
                                        <option value='Serbia'>Serbia</option>
                                        <option value='Seychelles'>Seychelles</option>
                                        <option value='Sierra Leone'>Sierra Leone</option>
                                        <option value='Singapore'>Singapore</option>
                                        <option value='Slovakia'>Slovakia</option>
                                        <option value='Slovenia'>Slovenia</option>
                                        <option value='Smaller Territories of the UK'>Smaller Territories of the UK</option>
                                        <option value='Solomon Islands'>Solomon Islands</option>
                                        <option value='Somalia'>Somalia</option>
                                        <option value='South Africa'>South Africa</option>
                                        <option value='South Georgia'>South Georgia</option>
                                        <option value='South Sudan'>South Sudan</option>
                                        <option value='Spain'>Spain</option>
                                        <option value='Sri Lanka'>Sri Lanka</option>
                                        <option value='Sudan'>Sudan</option>
                                        <option value='Suriname'>Suriname</option>
                                        <option value='Svalbard And Jan Mayen Islands'>Svalbard And Jan Mayen Islands</option>
                                        <option value='Swaziland'>Swaziland</option>
                                        <option value='Sweden'>Sweden</option>
                                        <option value='Switzerland'>Switzerland</option>
                                        <option value='Syria'>Syria</option>
                                        <option value='Taiwan'>Taiwan</option>
                                        <option value='Tajikistan'>Tajikistan</option>
                                        <option value='Tanzania'>Tanzania</option>
                                        <option value='Thailand'>Thailand</option>
                                        <option value='Togo'>Togo</option>
                                        <option value='Tokelau'>Tokelau</option>
                                        <option value='Tonga'>Tonga</option>
                                        <option value='Trinidad And Tobago'>Trinidad And Tobago</option>
                                        <option value='Tunisia'>Tunisia</option>
                                        <option value='Turkey'>Turkey</option>
                                        <option value='Turkmenistan'>Turkmenistan</option>
                                        <option value='Turks And Caicos Islands'>Turks And Caicos Islands</option>
                                        <option value='Tuvalu'>Tuvalu</option>
                                        <option value='Uganda'>Uganda</option>
                                        <option value='Ukraine'>Ukraine</option>
                                        <option value='United Arab Emirates'>United Arab Emirates</option>
                                        <option value='United Kingdom'>United Kingdom</option>
                                        <option value='United States'>United States</option>
                                        <option value='United States Minor Outlying Islands'>United States Minor Outlying Islands</option>
                                        <option value='Uruguay'>Uruguay</option>
                                        <option value='Uzbekistan'>Uzbekistan</option>
                                        <option value='Vanuatu'>Vanuatu</option>
                                        <option value='Vatican City State (Holy See)'>Vatican City State (Holy See)</option>
                                        <option value='Venezuela'>Venezuela</option>
                                        <option value='Vietnam'>Vietnam</option>
                                        <option value='Virgin Islands (British)'>Virgin Islands (British)</option>
                                        <option value='Virgin Islands (US)'>Virgin Islands (US)</option>
                                        <option value='Wallis And Futuna Islands'>Wallis And Futuna Islands</option>
                                        <option value='Western Sahara'>Western Sahara</option>
                                        <option value='Yemen'>Yemen</option>
                                        <option value='Yugoslavia'>Yugoslavia</option>
                                        <option value='Zambia'>Zambia</option>
                                        <option value='Zimbabwe'>Zimbabwe</option>

                                </select>

                                <span id="outofcountry" style="color:red"></span>
                            </div>

                            <div class="form-holder" id="in_con" style="display: block;">
                                <select class="form-control" id="india_country" name="2_country">

                                    <option value="INDIA" selected>INDIA</option>
                                </select>
                                <span id="error_india_country" style="color:red"></span>
                            </div>

                        </div>

                        <div id="place_birth" style="display: block;">

                            <div class="col-md-4 top-10">
                                <label>State <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <select class="form-control" id="contact_2_state" name="2_state" data-fv-notempty="" data-fv-notempty-message="The state is required" required="">
                                        <option value="" disabled selected>Select state</option>
                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                        <option value="Arunachal Pradesh">Arunachal Pradesh </option>
                                        <option value="Assam">Assam </option>
                                        <option value="Bihar">Bihar</option>
                                        <option value="Chhattisgarh">Chhattisgarh</option>
                                        <option value="Goa">Goa</option>
                                        <option value="Gujarat">Gujarat</option>
                                        <option value="Haryana">Haryana</option>
                                        <option value="Himachal Pradesh">Himachal Pradesh </option>
                                        <option value="Jammu and Kashmir">Jammu and Kashmir </option>
                                        <option value="Jharkhand">Jharkhand</option>
                                        <option value="Karnataka">Karnataka </option>
                                        <option value="Kerala">Kerala</option>
                                        <option value="Madhya Pradesh">Madhya Pradesh </option>
                                        <option value="Maharashtra">Maharashtra</option>
                                        <option value="Manipur">Manipur</option>
                                        <option value="Meghalaya">Meghalaya </option>
                                        <option value="Mizoram">Mizoram </option>
                                        <option value="Nagaland">Nagaland</option>
                                        <option value="Odisha">Odisha</option>
                                        <option value="Punjab">Punjab</option>
                                        <option value="Rajasthan">Rajasthan </option>
                                        <option value="Sikkim">Sikkim </option>
                                        <option value="Tamil Nadu">Tamil Nadu </option>
                                        <option value="Telangana">Telangana </option>
                                        <option value="Tripura">Tripura</option>
                                        <option value="Uttar Pradesh">Uttar Pradesh </option>
                                        <option value="Uttarakhand">Uttarakhand</option>
                                        <option value="West Bengal">West Bengal</option>
                                        <option value="Andaman and Nicobar">Andaman and Nicobar </option>
                                        <option value="Chandigarh">Chandigarh </option>
                                        <option value="Dadra Nagar Haveli">Dadra Nagar Haveli </option>
                                        <option value="Daman and Diu">Daman and Diu </option>
                                        <option value="Delhi">Delhi </option>
                                        <option value="Lakshadweep">Lakshadweep </option>
                                        <option value="Puducherry">Puducherry </option>
                                    </select>
                                    <div id="state2" style="color:red"></div>

                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>District <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input  class="form-control" id="contact_2_district" name="2_district" placeholder="District" required="">

                                    <div id="" style="color:red"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Citizenship of India by</label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_citizenship" name="2_citizenship">
                                    <option value="" selected="" disabled="">select field</option>
                                    <option value="Birth">Birth</option>
                                    <option value="Descent">Descent</option>
                                    <option value="Registration/ Naturalization">Registration/ Naturalization</option>
                                </select>
                                <span id="error_citizenship" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>PAN (If available)</label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="PAN" value="" id="txt_pan" name="2_pan" maxlength="10" style="text-transform:uppercase;">
                                <span id="error_pan" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Voter ID (If available)</label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="voter_id" value="" id="txt_voter_id" maxlength="10" style="text-transform:uppercase;">
                                <span id="error_voter" style="color:red"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4 top-10">
                            <label>Educational Qualification <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <select class="form-control" id="c_2_qualification" name="2_qualification" required="">
                                    <option value="" selected disabled>Select Educational Qualification</option>
                                    <option value="7th pass or less">7th pass or less</option>
                                    <option value="Between 8th and 9th standard">Between 8th and 9th standard</option>
                                    <option value="10th pass and above">10th pass and above</option>
                                    <option value="Graduate and above">Graduate and above</option>
                                </select>
                                <span id="qual" style="color:red"></span>
                            </div>
                        </div>

                        <div class="col-md-4 top-10">
                            <label>Employment Type <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_emp_type" name="2_employment_type" onchange="flip_emptype();" required="">

                                    <option value="" selected disabled>Select Employment Type</option>
                                    <option value="Government">Government</option>
                                    <option value="Homemaker">Homemaker</option>
                                    <option value="Not employed">Not employed</option>
                                    <option value="Others">Others</option>
                                    <option value="Owners, Partners &amp; Directors of companies">Owners, Partners &amp; Directors of companies </option>
                                    <option value="which are members of CII, FICCI &amp; ASSOCHAM">which are members of CII, FICCI &amp; ASSOCHAM</option>
                                    <option value="Private">Private</option>
                                    <option value="PSU">PSU</option>
                                    <option value="Retired Government Servant">Retired Government Servant</option>
                                    <option value="Retired- Private Service">Retired- Private Service</option>
                                    <option value="Self Employed">Self Employed</option>
                                    <option value="Statutory Body">Statutory Body</option>
                                    <option value="Student">Student</option>
                                </select>
                                <span class="error" id="mylocation70"></span>
                                <div id="emptype" style="color:red"></div>
                            </div>

                        </div>
                        <div id="div_emptype" style="display: none;">
                            <div class="col-md-4 top-10">
                                <label>Organisation Name <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Organisation Name" value="" id="txt_org_name" name="2_organization_name" maxlength="150">
                                    <span id="error_org" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Is your parent (If Minor)/ spouse, a govt servant? <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <select class="form-control" id="contact_2_sapouse" name="2_sapouse" required="">
                                    <option value="" selected disabled>Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <div id="sapouse" style="color:red"></div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Are you eligible for Non-ECR category? <span style="color:red">*</span><sup data-toggle="modal" data-target="#myModal"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup>
                            </label>
                            <div class="form-holder">
                                <select class="form-control" id="contact_2_non_ecr" name="2_non_ecr" required="">
                                    <option value="" selected disabled>Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <div id="nonecr" style="color:red"></div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Visible Distinguishing Mark (If any?) <sup data-toggle="modal" data-target="#myModal2"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup></label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Visible Distinguishing Mark" value="" id="txt_visible_dmark" name="2_body_mark" maxlength="100">
                                <span id="error_mark" style="color:red"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Are you known by any other names?</label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_any_aliases" name="2_alias_type" onchange="flip_aliases();">

                                    <option value="No" selected>No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span class="error" id="mylocation73"></span>
                            </div>
                        </div>
                    </div>

                    <div id="div_alises" style="display: none;">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Alias Name1, alises First name <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="Alise Name 1" value="" id="txt_aliases_fname" name="2_alias_fname" maxlength="50" >
                                    <span id="error_alf" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label> Middle name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="Middle Name" value="" id="txt_aliases_mname" name="2_alias_mname" maxlength="50">
                                    <span id="error_alm" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Surname</label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="Surname" value="" id="txt_aliases_lname" name="2_alias_lname" maxlength="50">
                                    <span id="error_all" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clb"></div>
                        <div class="col-md-12 topf label-input"></div>

                        <div class="clb"></div>
                        <div class="col-md-12 topf label-input"></div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Have you ever changed your name ? </label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_chg_name" name="2_previous_type" onchange="flip_chngname();">

                                    <option value="No" selected>No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span class="error" id="mylocation74"></span>
                            </div>
                        </div>

                    </div>

                    <div id="div_chngname" style="display: none;">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Previous Name1,alises First name <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="First Name" value="" id="2_previous_txt_fname" name="2_previous_fname" maxlength="50" >
                                    <span id="error_alpf" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Middle name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="Middle Name" value="" id="2_previous_txt_mname" name="2_previous_mname" maxlength="50">
                                    <span id="error_alpm" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Surname</label>
                                <div class="form-holder">
                                    <input type="text" class="form-control text-upper" placeholder="Surname" value="" id="2_previous_txt_lname" name="2_previous_lname" maxlength="50">
                                    <span id="error_alpl" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clb"></div>

                        <div class="clb"></div>
                        <div class="col-md-12 topf label-input"></div>
                    </div>

                </div>
            </div>
        </div>

        <!---->

<br>        
        <section class="section-padding" id="contact">
            <div class="container card">
                <div class="row white">
                    <div class="col-md-8 col-sm-12 section-head">
                        <div class="section-title">
                            <h4>3. Family Details (Father/Mother/Legal Guardian details; at least one is mandatory.)</h4>
                            <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">

                            <div class="col-md-4">
                                <label>Father's First Name <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Father's First Name" id="myform_3_father_f_name" name="3_father_f_name" maxlength="50" required="">
                                    <span id="error_myform_3_father_f_name" style="color:red"></span> 
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Middle Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Father's Middle Name" id="myform_3_father_m_name" name="3_father_m_name" maxlength="50">
                                    <span id="error_myform_3_father_m_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Surname </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Father's Surname" id="myform_3_father_l_name" name="3_father_l_name" maxlength="50">
                                    <span id="error_myform_3_father_l_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-12 topf label-input"></div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <label>Mother's First Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Mother's First Name" id="myform_3_mother_f_name" name="3_mother_f_name" maxlength="50">
                                    <span id="error_myform_3_mother_f_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Middle Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Mother's Middle Name" id="myform_3_mother_m_name" name="3_mother_m_name" maxlength="50">
                                    <span id="error_myform_3_mother_m_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Surname </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Mother's Surname" id="myform_3_mother_l_name" name="3_mother_l_name" maxlength="50">
                                    <span id="error_myform_3_mother_l_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-12 topf label-input">

                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4 top-10">
                                <label>Legal Guardian's First Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="First Name" id="myform_3_gard_f_name" name="3_gard_f_name" maxlength="50">
                                    <span id="error_myform_3_gard_f_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Middle Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Middle Name" id="myform_3_gard_m_name" name="3_gard_m_name" maxlength="50">
                                    <span id="error_myform_3_gard_m_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Surname </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Surname" id="myform_3_gard_l_name" name="3_gard_l_name" maxlength="50">
                                    <span id="error_myform_3_gard_l_name" style="color:red"></span>
                                </div>
                            </div>

                        </div>

                        <div class="row" id="flio_lbl_marital" style="display: none">
                            <div class="col-md-4 top-10">
                                <label>Spouse First Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="First Name" id="myform_3_spz_f_name" name="3_spz_f_name" maxlength="50">
                                    <span id="error_myform_3_spz_f_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Middle Name </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Middle Name" id="myform_3_spz_m_name" name="3_spz_m_name" maxlength="50">
                                    <span id="error_myform_3_spz_m_name" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Surname </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Surname" id="myform_3_spz_l_name" name="3_spz_l_name" maxlength="50">
                                    <span id="error_myform_3_spz_l_name" style="color:red"></span>
                                </div>
                            </div>
                        </div>

                        <div id="error_fathermother" style="color:red; text-align:center; padding-top: 20px;"></div>

                    </div>

                </div>
            </div>
        </section>

        <!--Section 4-->

<br>        

        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>4. Present Residential Address Details (where applicant presently resides)</h4>
                        <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">

                        <div class="col-md-4">
                            <label>Is your present address out of India? </label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_present_adr_1" name="4_present_address_type" onchange="flip_chng_pres_cntry_4();flip_labels();">

                                    <option value="No" selected>No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span id="error_sel_present_adr_1" style="color:red"></span>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>House No. and Street Name <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="House No. and Street Name" id="contact_4_house_no" name="4_house_no" maxlength="50" required="">
                                <div id="houseno" style="color:red"></div>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Village or Town or City <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Village or town or city " id="contact_4_town" name="4_town" maxlength="50" required="">
                                <div id="town" style="color:red"></div>
                            </div>

                        </div>
                        <div>
                            <div class="col-md-4">
                                <label>Country <span style="color:red">*</span> </label>
                                <div class="form-holder fssn" id="div_chng_pres_cntry_1" style="display: none;" >
                                    <select class="form-control" id="contact_14_country" name="4_country">
                                        <option value="" disabled selected> Select Country</option>
                                        <option value='Afghanistan'>Afghanistan</option>
                                        <option value='Albania'>Albania</option>
                                        <option value='Algeria'>Algeria</option>
                                        <option value='American Samoa'>American Samoa</option>
                                        <option value='Andorra'>Andorra</option>
                                        <option value='Angola'>Angola</option>
                                        <option value='Anguilla'>Anguilla</option>
                                        <option value='Antarctica'>Antarctica</option>
                                        <option value='Antigua And Barbuda'>Antigua And Barbuda</option>
                                        <option value='Argentina'>Argentina</option>
                                        <option value='Armenia'>Armenia</option>
                                        <option value='Aruba'>Aruba</option>
                                        <option value='Australia'>Australia</option>
                                        <option value='Austria'>Austria</option>
                                        <option value='Azerbaijan'>Azerbaijan</option>
                                        <option value='Bahamas The'>Bahamas The</option>
                                        <option value='Bahrain'>Bahrain</option>
                                        <option value='Bangladesh'>Bangladesh</option>
                                        <option value='Barbados'>Barbados</option>
                                        <option value='Belarus'>Belarus</option>
                                        <option value='Belgium'>Belgium</option>
                                        <option value='Belize'>Belize</option>
                                        <option value='Benin'>Benin</option>
                                        <option value='Bermuda'>Bermuda</option>
                                        <option value='Bhutan'>Bhutan</option>
                                        <option value='Bolivia'>Bolivia</option>
                                        <option value='Bosnia and Herzegovina'>Bosnia and Herzegovina</option>
                                        <option value='Botswana'>Botswana</option>
                                        <option value='Bouvet Island'>Bouvet Island</option>
                                        <option value='Brazil'>Brazil</option>
                                        <option value='British Indian Ocean Territory'>British Indian Ocean Territory</option>
                                        <option value='Brunei'>Brunei</option>
                                        <option value='Bulgaria'>Bulgaria</option>
                                        <option value='Burkina Faso'>Burkina Faso</option>
                                        <option value='Burundi'>Burundi</option>
                                        <option value='Cambodia'>Cambodia</option>
                                        <option value='Cameroon'>Cameroon</option>
                                        <option value='Canada'>Canada</option>
                                        <option value='Cape Verde'>Cape Verde</option>
                                        <option value='Cayman Islands'>Cayman Islands</option>
                                        <option value='Central African Republic'>Central African Republic</option>
                                        <option value='Chad'>Chad</option>
                                        <option value='Chile'>Chile</option>
                                        <option value='China'>China</option>
                                        <option value='Christmas Island'>Christmas Island</option>
                                        <option value='Cocos (Keeling) Islands'>Cocos (Keeling) Islands</option>
                                        <option value='Colombia'>Colombia</option>
                                        <option value='Comoros'>Comoros</option>
                                        <option value='Republic Of The Congo'>Republic Of The Congo</option>
                                        <option value='Democratic Republic Of The Congo'>Democratic Republic Of The Congo</option>
                                        <option value='Cook Islands'>Cook Islands</option>
                                        <option value='Costa Rica'>Costa Rica</option>
                                        <option value='Cote D' Ivoire (Ivory Coast) '>Cote D'Ivoire (Ivory Coast)</option>
                                            <option value='Croatia (Hrvatska)'>Croatia (Hrvatska)</option>
                                            <option value='Cuba'>Cuba</option>
                                            <option value='Cyprus'>Cyprus</option>
                                            <option value='Czech Republic'>Czech Republic</option>
                                            <option value='Denmark'>Denmark</option>
                                            <option value='Djibouti'>Djibouti</option>
                                            <option value='Dominica'>Dominica</option>
                                            <option value='Dominican Republic'>Dominican Republic</option>
                                            <option value='East Timor'>East Timor</option>
                                            <option value='Ecuador'>Ecuador</option>
                                            <option value='Egypt'>Egypt</option>
                                            <option value='El Salvador'>El Salvador</option>
                                            <option value='Equatorial Guinea'>Equatorial Guinea</option>
                                            <option value='Eritrea'>Eritrea</option>
                                            <option value='Estonia'>Estonia</option>
                                            <option value='Ethiopia'>Ethiopia</option>
                                            <option value='External Territories of Australia'>External Territories of Australia</option>
                                            <option value='Falkland Islands'>Falkland Islands</option>
                                            <option value='Faroe Islands'>Faroe Islands</option>
                                            <option value='Fiji Islands'>Fiji Islands</option>
                                            <option value='Finland'>Finland</option>
                                            <option value='France'>France</option>
                                            <option value='French Guiana'>French Guiana</option>
                                            <option value='French Polynesia'>French Polynesia</option>
                                            <option value='French Southern Territories'>French Southern Territories</option>
                                            <option value='Gabon'>Gabon</option>
                                            <option value='Gambia The'>Gambia The</option>
                                            <option value='Georgia'>Georgia</option>
                                            <option value='Germany'>Germany</option>
                                            <option value='Ghana'>Ghana</option>
                                            <option value='Gibraltar'>Gibraltar</option>
                                            <option value='Greece'>Greece</option>
                                            <option value='Greenland'>Greenland</option>
                                            <option value='Grenada'>Grenada</option>
                                            <option value='Guadeloupe'>Guadeloupe</option>
                                            <option value='Guam'>Guam</option>
                                            <option value='Guatemala'>Guatemala</option>
                                            <option value='Guernsey and Alderney'>Guernsey and Alderney</option>
                                            <option value='Guinea'>Guinea</option>
                                            <option value='Guinea-Bissau'>Guinea-Bissau</option>
                                            <option value='Guyana'>Guyana</option>
                                            <option value='Haiti'>Haiti</option>
                                            <option value='Heard and McDonald Islands'>Heard and McDonald Islands</option>
                                            <option value='Honduras'>Honduras</option>
                                            <option value='Hong Kong S.A.R.'>Hong Kong S.A.R.</option>
                                            <option value='Hungary'>Hungary</option>
                                            <option value='Iceland'>Iceland</option>
                                            <option value='Indonesia'>Indonesia</option>
                                            <option value='Iran'>Iran</option>
                                            <option value='Iraq'>Iraq</option>
                                            <option value='Ireland'>Ireland</option>
                                            <option value='Israel'>Israel</option>
                                            <option value='Italy'>Italy</option>
                                            <option value='Jamaica'>Jamaica</option>
                                            <option value='Japan'>Japan</option>
                                            <option value='Jersey'>Jersey</option>
                                            <option value='Jordan'>Jordan</option>
                                            <option value='Kazakhstan'>Kazakhstan</option>
                                            <option value='Kenya'>Kenya</option>
                                            <option value='Kiribati'>Kiribati</option>
                                            <option value='Korea North'>Korea North</option>
                                            <option value='Korea South'>Korea South</option>
                                            <option value='Kuwait'>Kuwait</option>
                                            <option value='Kyrgyzstan'>Kyrgyzstan</option>
                                            <option value='Laos'>Laos</option>
                                            <option value='Latvia'>Latvia</option>
                                            <option value='Lebanon'>Lebanon</option>
                                            <option value='Lesotho'>Lesotho</option>
                                            <option value='Liberia'>Liberia</option>
                                            <option value='Libya'>Libya</option>
                                            <option value='Liechtenstein'>Liechtenstein</option>
                                            <option value='Lithuania'>Lithuania</option>
                                            <option value='Luxembourg'>Luxembourg</option>
                                            <option value='Macau S.A.R.'>Macau S.A.R.</option>
                                            <option value='Macedonia'>Macedonia</option>
                                            <option value='Madagascar'>Madagascar</option>
                                            <option value='Malawi'>Malawi</option>
                                            <option value='Malaysia'>Malaysia</option>
                                            <option value='Maldives'>Maldives</option>
                                            <option value='Mali'>Mali</option>
                                            <option value='Malta'>Malta</option>
                                            <option value='Man (Isle of)'>Man (Isle of)</option>
                                            <option value='Marshall Islands'>Marshall Islands</option>
                                            <option value='Martinique'>Martinique</option>
                                            <option value='Mauritania'>Mauritania</option>
                                            <option value='Mauritius'>Mauritius</option>
                                            <option value='Mayotte'>Mayotte</option>
                                            <option value='Mexico'>Mexico</option>
                                            <option value='Micronesia'>Micronesia</option>
                                            <option value='Moldova'>Moldova</option>
                                            <option value='Monaco'>Monaco</option>
                                            <option value='Mongolia'>Mongolia</option>
                                            <option value='Montserrat'>Montserrat</option>
                                            <option value='Morocco'>Morocco</option>
                                            <option value='Mozambique'>Mozambique</option>
                                            <option value='Myanmar'>Myanmar</option>
                                            <option value='Namibia'>Namibia</option>
                                            <option value='Nauru'>Nauru</option>
                                            <option value='Nepal'>Nepal</option>
                                            <option value='Netherlands Antilles'>Netherlands Antilles</option>
                                            <option value='Netherlands The'>Netherlands The</option>
                                            <option value='New Caledonia'>New Caledonia</option>
                                            <option value='New Zealand'>New Zealand</option>
                                            <option value='Nicaragua'>Nicaragua</option>
                                            <option value='Niger'>Niger</option>
                                            <option value='Nigeria'>Nigeria</option>
                                            <option value='Niue'>Niue</option>
                                            <option value='Norfolk Island'>Norfolk Island</option>
                                            <option value='Northern Mariana Islands'>Northern Mariana Islands</option>
                                            <option value='Norway'>Norway</option>
                                            <option value='Oman'>Oman</option>
                                            <option value='Pakistan'>Pakistan</option>
                                            <option value='Palau'>Palau</option>
                                            <option value='Palestinian Territory Occupied'>Palestinian Territory Occupied</option>
                                            <option value='Panama'>Panama</option>
                                            <option value='Papua new Guinea'>Papua new Guinea</option>
                                            <option value='Paraguay'>Paraguay</option>
                                            <option value='Peru'>Peru</option>
                                            <option value='Philippines'>Philippines</option>
                                            <option value='Pitcairn Island'>Pitcairn Island</option>
                                            <option value='Poland'>Poland</option>
                                            <option value='Portugal'>Portugal</option>
                                            <option value='Puerto Rico'>Puerto Rico</option>
                                            <option value='Qatar'>Qatar</option>
                                            <option value='Reunion'>Reunion</option>
                                            <option value='Romania'>Romania</option>
                                            <option value='Russia'>Russia</option>
                                            <option value='Rwanda'>Rwanda</option>
                                            <option value='Saint Helena'>Saint Helena</option>
                                            <option value='Saint Kitts And Nevis'>Saint Kitts And Nevis</option>
                                            <option value='Saint Lucia'>Saint Lucia</option>
                                            <option value='Saint Pierre and Miquelon'>Saint Pierre and Miquelon</option>
                                            <option value='Saint Vincent And The Grenadines'>Saint Vincent And The Grenadines</option>
                                            <option value='Samoa'>Samoa</option>
                                            <option value='San Marino'>San Marino</option>
                                            <option value='Sao Tome and Principe'>Sao Tome and Principe</option>
                                            <option value='Saudi Arabia'>Saudi Arabia</option>
                                            <option value='Senegal'>Senegal</option>
                                            <option value='Serbia'>Serbia</option>
                                            <option value='Seychelles'>Seychelles</option>
                                            <option value='Sierra Leone'>Sierra Leone</option>
                                            <option value='Singapore'>Singapore</option>
                                            <option value='Slovakia'>Slovakia</option>
                                            <option value='Slovenia'>Slovenia</option>
                                            <option value='Smaller Territories of the UK'>Smaller Territories of the UK</option>
                                            <option value='Solomon Islands'>Solomon Islands</option>
                                            <option value='Somalia'>Somalia</option>
                                            <option value='South Africa'>South Africa</option>
                                            <option value='South Georgia'>South Georgia</option>
                                            <option value='South Sudan'>South Sudan</option>
                                            <option value='Spain'>Spain</option>
                                            <option value='Sri Lanka'>Sri Lanka</option>
                                            <option value='Sudan'>Sudan</option>
                                            <option value='Suriname'>Suriname</option>
                                            <option value='Svalbard And Jan Mayen Islands'>Svalbard And Jan Mayen Islands</option>
                                            <option value='Swaziland'>Swaziland</option>
                                            <option value='Sweden'>Sweden</option>
                                            <option value='Switzerland'>Switzerland</option>
                                            <option value='Syria'>Syria</option>
                                            <option value='Taiwan'>Taiwan</option>
                                            <option value='Tajikistan'>Tajikistan</option>
                                            <option value='Tanzania'>Tanzania</option>
                                            <option value='Thailand'>Thailand</option>
                                            <option value='Togo'>Togo</option>
                                            <option value='Tokelau'>Tokelau</option>
                                            <option value='Tonga'>Tonga</option>
                                            <option value='Trinidad And Tobago'>Trinidad And Tobago</option>
                                            <option value='Tunisia'>Tunisia</option>
                                            <option value='Turkey'>Turkey</option>
                                            <option value='Turkmenistan'>Turkmenistan</option>
                                            <option value='Turks And Caicos Islands'>Turks And Caicos Islands</option>
                                            <option value='Tuvalu'>Tuvalu</option>
                                            <option value='Uganda'>Uganda</option>
                                            <option value='Ukraine'>Ukraine</option>
                                            <option value='United Arab Emirates'>United Arab Emirates</option>
                                            <option value='United Kingdom'>United Kingdom</option>
                                            <option value='United States'>United States</option>
                                            <option value='United States Minor Outlying Islands'>United States Minor Outlying Islands</option>
                                            <option value='Uruguay'>Uruguay</option>
                                            <option value='Uzbekistan'>Uzbekistan</option>
                                            <option value='Vanuatu'>Vanuatu</option>
                                            <option value='Vatican City State (Holy See)'>Vatican City State (Holy See)</option>
                                            <option value='Venezuela'>Venezuela</option>
                                            <option value='Vietnam'>Vietnam</option>
                                            <option value='Virgin Islands (British)'>Virgin Islands (British)</option>
                                            <option value='Virgin Islands (US)'>Virgin Islands (US)</option>
                                            <option value='Wallis And Futuna Islands'>Wallis And Futuna Islands</option>
                                            <option value='Western Sahara'>Western Sahara</option>
                                            <option value='Yemen'>Yemen</option>
                                            <option value='Yugoslavia'>Yugoslavia</option>
                                            <option value='Zambia'>Zambia</option>
                                            <option value='Zimbabwe'>Zimbabwe</option>
                                    </select>
                                    <div id="country1" style="color:red"></div>
                                </div>
                                <div class="form-holder mdds" id="div_chng_pres_cntry_1" style="display: block;">
                                    <!--  <input type="text" class="form-control" value="India" id="country14" name="4_country" readonly>-->
                                    <select class="form-control" id="contact_4_country" name="4_country">
                                        <option value="INDIA"> INDIA</option>

                                    </select>
                                    <div id="error_country1" style="color:red"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-4 top-10 ">
                            <label>State/ Province <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="State/ Province" id="contact_4_state" name="4_state" maxlength="100" required="">
                                <div id="fstate" style="color:red"></div>
                            </div>
                        </div>
                        <div class="col-md-4 top-10 ">
                            <label>District <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="District" id="contact_4_district" name="4_district" maxlength="100" required="">
                                <div id="fdistrict" style="color:red"></div>
                            </div>

                        </div>

                        <div class="col-md-4 top-10">
                            <label>
                                <div class="label-input">Pin Code / Zip Code<span style="color:red">*</span></div>
                            </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Pin" value="" id="contact_4_pincode" name="4_pincode" maxlength="6" required="">
                                <div id="error_zipcode" style="color:red"></div>
                            </div>

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Police Station <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Police station" id="contact_4_police_station" name="4_police_station" maxlength="40" required="">
                                <div id="police" style="color:red"></div>
                            </div>

                            <p class="marq-blink text-center" style="font-size: 12px;"><b>(Police Station may vary than you mentioned.)</b></p>
                        </div>
                        <div class="col-md-4">
                            <label>Mobile No <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control" placeholder="Mobile No. (10 Digit)" id="contact_4_mobile" name="4_mobile" maxlength="10" required="">
                                <div id="error_contact" style="color:red"></div> 
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>E-mail ID <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="email" class="form-control" placeholder="E-mail ID" id="contact_4_email" name="4_email" maxlength="30" required="">
                                <div id="error_email" style="color:red"></div>
                            </div>

                        </div>

                    </div>

                <!--    <div class="row">

                        <div class="col-md-4 top-10">
                            <label>Do you have a Permanent Address? </label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_permanent_adr" name="4_permanent_address" onchange="flip_perm_yn_adr();">

                                    <option value="No" selected>No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span class="error" id="mylocation77"></span>
                            </div>
                        </div>

                    </div>   -->

                    <div id="div_perm_yn_adr" style="display: block;">
                        <div class="clb"></div>
                        <div class="row">
                            <div class="col-md-4 top-10">
                                <label>Is permanent address same as present address? </label>
                                <div class="form-holder">
                                    <select class="form-control" id="sel_same_res_adr" name="4_permanent_address_type" onchange="flip_permenant_adr();">

                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clb"></div>
                        <div id="div_permanent_adr" style="display:none;">
                            <div class="row">
                                <div class="col-md-4 top-10">
                                    <label>House No. and Street Name <span style="color:red">*</span> </label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="House No. and Street Name" value="" id="txt_adr_per_house" name="4_p_house_no" >
                                        <span id="error_txt_adr_per_house" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="col-md-4 top-10">
                                    <label>Village or Town or City <span style="color:red">*</span> </label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="Village or Town or City" value="" id="txt_adr_per_city" name="4_p_town" >
                                        <span id="error_txt_adr_per_city" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="col-md-4 top-10">
                                    <label>Country <span class="marq-blink text-center" style="font-size: 12px;"><b>(If not in India.)</b></span></label>
                                    <div class="form-holder">
                                        <select class="form-control" id="txt_adr_per_country" name="4_p_country">
                                            <option value="" disabled selected> Select</option>
                                            <option value='Afghanistan'>Afghanistan</option>
                                            <option value='Albania'>Albania</option>
                                            <option value='Algeria'>Algeria</option>
                                            <option value='American Samoa'>American Samoa</option>
                                            <option value='Andorra'>Andorra</option>
                                            <option value='Angola'>Angola</option>
                                            <option value='Anguilla'>Anguilla</option>
                                            <option value='Antarctica'>Antarctica</option>
                                            <option value='Antigua And Barbuda'>Antigua And Barbuda</option>
                                            <option value='Argentina'>Argentina</option>
                                            <option value='Armenia'>Armenia</option>
                                            <option value='Aruba'>Aruba</option>
                                            <option value='Australia'>Australia</option>
                                            <option value='Austria'>Austria</option>
                                            <option value='Azerbaijan'>Azerbaijan</option>
                                            <option value='Bahamas The'>Bahamas The</option>
                                            <option value='Bahrain'>Bahrain</option>
                                            <option value='Bangladesh'>Bangladesh</option>
                                            <option value='Barbados'>Barbados</option>
                                            <option value='Belarus'>Belarus</option>
                                            <option value='Belgium'>Belgium</option>
                                            <option value='Belize'>Belize</option>
                                            <option value='Benin'>Benin</option>
                                            <option value='Bermuda'>Bermuda</option>
                                            <option value='Bhutan'>Bhutan</option>
                                            <option value='Bolivia'>Bolivia</option>
                                            <option value='Bosnia and Herzegovina'>Bosnia and Herzegovina</option>
                                            <option value='Botswana'>Botswana</option>
                                            <option value='Bouvet Island'>Bouvet Island</option>
                                            <option value='Brazil'>Brazil</option>
                                            <option value='British Indian Ocean Territory'>British Indian Ocean Territory</option>
                                            <option value='Brunei'>Brunei</option>
                                            <option value='Bulgaria'>Bulgaria</option>
                                            <option value='Burkina Faso'>Burkina Faso</option>
                                            <option value='Burundi'>Burundi</option>
                                            <option value='Cambodia'>Cambodia</option>
                                            <option value='Cameroon'>Cameroon</option>
                                            <option value='Canada'>Canada</option>
                                            <option value='Cape Verde'>Cape Verde</option>
                                            <option value='Cayman Islands'>Cayman Islands</option>
                                            <option value='Central African Republic'>Central African Republic</option>
                                            <option value='Chad'>Chad</option>
                                            <option value='Chile'>Chile</option>
                                            <option value='China'>China</option>
                                            <option value='Christmas Island'>Christmas Island</option>
                                            <option value='Cocos (Keeling) Islands'>Cocos (Keeling) Islands</option>
                                            <option value='Colombia'>Colombia</option>
                                            <option value='Comoros'>Comoros</option>
                                            <option value='Republic Of The Congo'>Republic Of The Congo</option>
                                            <option value='Democratic Republic Of The Congo'>Democratic Republic Of The Congo</option>
                                            <option value='Cook Islands'>Cook Islands</option>
                                            <option value='Costa Rica'>Costa Rica</option>
                                            <option value='Cote D' Ivoire (Ivory Coast) '>Cote D'Ivoire (Ivory Coast)</option>
                                                <option value='Croatia (Hrvatska)'>Croatia (Hrvatska)</option>
                                                <option value='Cuba'>Cuba</option>
                                                <option value='Cyprus'>Cyprus</option>
                                                <option value='Czech Republic'>Czech Republic</option>
                                                <option value='Denmark'>Denmark</option>
                                                <option value='Djibouti'>Djibouti</option>
                                                <option value='Dominica'>Dominica</option>
                                                <option value='Dominican Republic'>Dominican Republic</option>
                                                <option value='East Timor'>East Timor</option>
                                                <option value='Ecuador'>Ecuador</option>
                                                <option value='Egypt'>Egypt</option>
                                                <option value='El Salvador'>El Salvador</option>
                                                <option value='Equatorial Guinea'>Equatorial Guinea</option>
                                                <option value='Eritrea'>Eritrea</option>
                                                <option value='Estonia'>Estonia</option>
                                                <option value='Ethiopia'>Ethiopia</option>
                                                <option value='External Territories of Australia'>External Territories of Australia</option>
                                                <option value='Falkland Islands'>Falkland Islands</option>
                                                <option value='Faroe Islands'>Faroe Islands</option>
                                                <option value='Fiji Islands'>Fiji Islands</option>
                                                <option value='Finland'>Finland</option>
                                                <option value='France'>France</option>
                                                <option value='French Guiana'>French Guiana</option>
                                                <option value='French Polynesia'>French Polynesia</option>
                                                <option value='French Southern Territories'>French Southern Territories</option>
                                                <option value='Gabon'>Gabon</option>
                                                <option value='Gambia The'>Gambia The</option>
                                                <option value='Georgia'>Georgia</option>
                                                <option value='Germany'>Germany</option>
                                                <option value='Ghana'>Ghana</option>
                                                <option value='Gibraltar'>Gibraltar</option>
                                                <option value='Greece'>Greece</option>
                                                <option value='Greenland'>Greenland</option>
                                                <option value='Grenada'>Grenada</option>
                                                <option value='Guadeloupe'>Guadeloupe</option>
                                                <option value='Guam'>Guam</option>
                                                <option value='Guatemala'>Guatemala</option>
                                                <option value='Guernsey and Alderney'>Guernsey and Alderney</option>
                                                <option value='Guinea'>Guinea</option>
                                                <option value='Guinea-Bissau'>Guinea-Bissau</option>
                                                <option value='Guyana'>Guyana</option>
                                                <option value='Haiti'>Haiti</option>
                                                <option value='Heard and McDonald Islands'>Heard and McDonald Islands</option>
                                                <option value='Honduras'>Honduras</option>
                                                <option value='Hong Kong S.A.R.'>Hong Kong S.A.R.</option>
                                                <option value='Hungary'>Hungary</option>
                                                <option value='Iceland'>Iceland</option>
                                                <option value='Indonesia'>Indonesia</option>
                                                <option value='Iran'>Iran</option>
                                                <option value='Iraq'>Iraq</option>
                                                <option value='Ireland'>Ireland</option>
                                                <option value='Israel'>Israel</option>
                                                <option value='Italy'>Italy</option>
                                                <option value='Jamaica'>Jamaica</option>
                                                <option value='Japan'>Japan</option>
                                                <option value='Jersey'>Jersey</option>
                                                <option value='Jordan'>Jordan</option>
                                                <option value='Kazakhstan'>Kazakhstan</option>
                                                <option value='Kenya'>Kenya</option>
                                                <option value='Kiribati'>Kiribati</option>
                                                <option value='Korea North'>Korea North</option>
                                                <option value='Korea South'>Korea South</option>
                                                <option value='Kuwait'>Kuwait</option>
                                                <option value='Kyrgyzstan'>Kyrgyzstan</option>
                                                <option value='Laos'>Laos</option>
                                                <option value='Latvia'>Latvia</option>
                                                <option value='Lebanon'>Lebanon</option>
                                                <option value='Lesotho'>Lesotho</option>
                                                <option value='Liberia'>Liberia</option>
                                                <option value='Libya'>Libya</option>
                                                <option value='Liechtenstein'>Liechtenstein</option>
                                                <option value='Lithuania'>Lithuania</option>
                                                <option value='Luxembourg'>Luxembourg</option>
                                                <option value='Macau S.A.R.'>Macau S.A.R.</option>
                                                <option value='Macedonia'>Macedonia</option>
                                                <option value='Madagascar'>Madagascar</option>
                                                <option value='Malawi'>Malawi</option>
                                                <option value='Malaysia'>Malaysia</option>
                                                <option value='Maldives'>Maldives</option>
                                                <option value='Mali'>Mali</option>
                                                <option value='Malta'>Malta</option>
                                                <option value='Man (Isle of)'>Man (Isle of)</option>
                                                <option value='Marshall Islands'>Marshall Islands</option>
                                                <option value='Martinique'>Martinique</option>
                                                <option value='Mauritania'>Mauritania</option>
                                                <option value='Mauritius'>Mauritius</option>
                                                <option value='Mayotte'>Mayotte</option>
                                                <option value='Mexico'>Mexico</option>
                                                <option value='Micronesia'>Micronesia</option>
                                                <option value='Moldova'>Moldova</option>
                                                <option value='Monaco'>Monaco</option>
                                                <option value='Mongolia'>Mongolia</option>
                                                <option value='Montserrat'>Montserrat</option>
                                                <option value='Morocco'>Morocco</option>
                                                <option value='Mozambique'>Mozambique</option>
                                                <option value='Myanmar'>Myanmar</option>
                                                <option value='Namibia'>Namibia</option>
                                                <option value='Nauru'>Nauru</option>
                                                <option value='Nepal'>Nepal</option>
                                                <option value='Netherlands Antilles'>Netherlands Antilles</option>
                                                <option value='Netherlands The'>Netherlands The</option>
                                                <option value='New Caledonia'>New Caledonia</option>
                                                <option value='New Zealand'>New Zealand</option>
                                                <option value='Nicaragua'>Nicaragua</option>
                                                <option value='Niger'>Niger</option>
                                                <option value='Nigeria'>Nigeria</option>
                                                <option value='Niue'>Niue</option>
                                                <option value='Norfolk Island'>Norfolk Island</option>
                                                <option value='Northern Mariana Islands'>Northern Mariana Islands</option>
                                                <option value='Norway'>Norway</option>
                                                <option value='Oman'>Oman</option>
                                                <option value='Pakistan'>Pakistan</option>
                                                <option value='Palau'>Palau</option>
                                                <option value='Palestinian Territory Occupied'>Palestinian Territory Occupied</option>
                                                <option value='Panama'>Panama</option>
                                                <option value='Papua new Guinea'>Papua new Guinea</option>
                                                <option value='Paraguay'>Paraguay</option>
                                                <option value='Peru'>Peru</option>
                                                <option value='Philippines'>Philippines</option>
                                                <option value='Pitcairn Island'>Pitcairn Island</option>
                                                <option value='Poland'>Poland</option>
                                                <option value='Portugal'>Portugal</option>
                                                <option value='Puerto Rico'>Puerto Rico</option>
                                                <option value='Qatar'>Qatar</option>
                                                <option value='Reunion'>Reunion</option>
                                                <option value='Romania'>Romania</option>
                                                <option value='Russia'>Russia</option>
                                                <option value='Rwanda'>Rwanda</option>
                                                <option value='Saint Helena'>Saint Helena</option>
                                                <option value='Saint Kitts And Nevis'>Saint Kitts And Nevis</option>
                                                <option value='Saint Lucia'>Saint Lucia</option>
                                                <option value='Saint Pierre and Miquelon'>Saint Pierre and Miquelon</option>
                                                <option value='Saint Vincent And The Grenadines'>Saint Vincent And The Grenadines</option>
                                                <option value='Samoa'>Samoa</option>
                                                <option value='San Marino'>San Marino</option>
                                                <option value='Sao Tome and Principe'>Sao Tome and Principe</option>
                                                <option value='Saudi Arabia'>Saudi Arabia</option>
                                                <option value='Senegal'>Senegal</option>
                                                <option value='Serbia'>Serbia</option>
                                                <option value='Seychelles'>Seychelles</option>
                                                <option value='Sierra Leone'>Sierra Leone</option>
                                                <option value='Singapore'>Singapore</option>
                                                <option value='Slovakia'>Slovakia</option>
                                                <option value='Slovenia'>Slovenia</option>
                                                <option value='Smaller Territories of the UK'>Smaller Territories of the UK</option>
                                                <option value='Solomon Islands'>Solomon Islands</option>
                                                <option value='Somalia'>Somalia</option>
                                                <option value='South Africa'>South Africa</option>
                                                <option value='South Georgia'>South Georgia</option>
                                                <option value='South Sudan'>South Sudan</option>
                                                <option value='Spain'>Spain</option>
                                                <option value='Sri Lanka'>Sri Lanka</option>
                                                <option value='Sudan'>Sudan</option>
                                                <option value='Suriname'>Suriname</option>
                                                <option value='Svalbard And Jan Mayen Islands'>Svalbard And Jan Mayen Islands</option>
                                                <option value='Swaziland'>Swaziland</option>
                                                <option value='Sweden'>Sweden</option>
                                                <option value='Switzerland'>Switzerland</option>
                                                <option value='Syria'>Syria</option>
                                                <option value='Taiwan'>Taiwan</option>
                                                <option value='Tajikistan'>Tajikistan</option>
                                                <option value='Tanzania'>Tanzania</option>
                                                <option value='Thailand'>Thailand</option>
                                                <option value='Togo'>Togo</option>
                                                <option value='Tokelau'>Tokelau</option>
                                                <option value='Tonga'>Tonga</option>
                                                <option value='Trinidad And Tobago'>Trinidad And Tobago</option>
                                                <option value='Tunisia'>Tunisia</option>
                                                <option value='Turkey'>Turkey</option>
                                                <option value='Turkmenistan'>Turkmenistan</option>
                                                <option value='Turks And Caicos Islands'>Turks And Caicos Islands</option>
                                                <option value='Tuvalu'>Tuvalu</option>
                                                <option value='Uganda'>Uganda</option>
                                                <option value='Ukraine'>Ukraine</option>
                                                <option value='United Arab Emirates'>United Arab Emirates</option>
                                                <option value='United Kingdom'>United Kingdom</option>
                                                <option value='United States'>United States</option>
                                                <option value='United States Minor Outlying Islands'>United States Minor Outlying Islands</option>
                                                <option value='Uruguay'>Uruguay</option>
                                                <option value='Uzbekistan'>Uzbekistan</option>
                                                <option value='Vanuatu'>Vanuatu</option>
                                                <option value='Vatican City State (Holy See)'>Vatican City State (Holy See)</option>
                                                <option value='Venezuela'>Venezuela</option>
                                                <option value='Vietnam'>Vietnam</option>
                                                <option value='Virgin Islands (British)'>Virgin Islands (British)</option>
                                                <option value='Virgin Islands (US)'>Virgin Islands (US)</option>
                                                <option value='Wallis And Futuna Islands'>Wallis And Futuna Islands</option>
                                                <option value='Western Sahara'>Western Sahara</option>
                                                <option value='Yemen'>Yemen</option>
                                                <option value='Yugoslavia'>Yugoslavia</option>
                                                <option value='Zambia'>Zambia</option>
                                                <option value='Zimbabwe'>Zimbabwe</option>
                                        </select>
                                        <span id="error_txt_adr_per_country" style="color:red"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clb"></div>
                            <div class="row">
                                <div class="col-md-4 top-10 per_mds" style="display: block;">
                                    <label>State/ Province <span style="color:red">*</span></label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="State/ Province" id="state41" name="4_p_state" maxlength="100" >
                                        <span id="error_state41" style="color:red"></span>
                                    </div>
                                </div>
                                <div class="col-md-4 top-10 per_mds" style="display: block;">
                                    <label>District <span style="color:red">*</span></label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="State/ Province" id="dist41" name="4_p_district" maxlength="100" >
                                        <span id="error_dist41" style="color:red"></span>
                                    </div>
                                </div>

                                <div class="col-md-4 top-10">
                                    <label>Pin <span style="color:red">*</span> </label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="Pin" value="" id="txt_adr_per_pin" name="4_p_pincode" maxlength="6" >
                                        <span id="error_txt_adr_per_pin" style="color:red"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clb"></div>
                            <div class="row">
                                <div class="col-md-4 top-10">
                                    <label>Police Station <span style="color:red">* </span></label>
                                    <div class="form-holder">
                                        <input type="text" class="form-control" placeholder="Police station" value="" id="txt_adr_per_Pstation" name="4_p_police_station" maxlength="40" >
                                        <span id="error_txt_adr_per_Pstation" style="color:red"></span>
                                    </div>

                                    <span class="marq-blink text-center" style="font-size: 12px;"><b>(Police Station may vary than you mentioned.)</b></span>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--  -->
<br>

        <section class="section-padding" id="contact">
            <div class="container card">
                <div class="row white">
                    <div class="col-md-8 col-sm-12 section-head">
                        <div class="section-title">
                            <h4>5. Emergency Contact Details</h4>
                            <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-holder">
                                    <label for="male">Name And Address<span style="color:red">*</span></label>
                                    <textarea type="text" placeholder="Name And Address" class="form-control" id="contact_5_name_address" name="5_name_address" required="" ></textarea>
                                    <span id="error_contact_5" style="color:red"></span>
                                </div>

                            </div>
                            <div class="col-lg-4">
                                <div class="form-holder">
                                    <label for="male">Mobile No<span style="color:red">*</span></label>
                                    <input type="text" placeholder="Mobile No.(10 Digit)" class="form-control" id="contact_5_mobile" name="5_mobile" maxlength="10"  required="">
                                    <span id="error_contact5" style="color:red"></span>
                                </div>

                            </div>
                            <div class="col-lg-4">
                                <div class="form-holder">
                                    <label for="male">E-mail ID</label>
                                    <input type="text" placeholder="E-mail ID" class="form-control" id="email5" name="5_email" maxlength="30" >
                                    <span id="error_5_email" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<br>

        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>6. Previous Passport/ Application Details</h4>
                        <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <label>Have you ever held/hold any Identity Certificate? <span style="color:red">*</span> <sup data-toggle="modal" data-target="#myModal3"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup></label>
                            <div class="form-holder">
                                <select class="form-control" id="sel_identity_cer" name="6_id_certify_type" onchange="flip_chngIC();">
                                    <option value="" selected disabled>select field</option>
                                    <option value="No" selected="">No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                                <span id="error_sel_identity_cer"></span>
                            </div>
                        </div>

                    </div>

                    <div class="clb"></div>

                    <div id="div_changeIC" style="display: none;">
                        <div class="row">
                            <div class="col-md-4 top-10">
                                <label>Identity Certificate/Passport Number <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Identity Certificate/Passport Number " value="" id="txt_identity_cer_no" name="6_passport_no" maxlength="8" onkeyup="idcard61();" style="text-transform:uppercase;">
                                    <span id="error_txt_identity_cer_no" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Date of Issue(DD-MM-YYYY) <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="date" class="form-control date" placeholder="DD-MM-YYYY" value="" id="txt_cer_issue_date" name="6_date_of_issue">
                                    <span id="error_txt_cer_issue_date" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Date of Expiry(DD-MM-YYYY) <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="date" class="form-control date" placeholder="DD-MM-YYYY" value="" id="txt_cer_exp_date" name="6_date_of_expiry">
                                    <span id="error_txt_cer_exp_date" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 top-10">
                                <label>Place of Issue <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="Place of issue" value="" id="txt_cert_place_issue" name="6_place_issue" maxlength="50">
                                    <span id="error_txt_cert_place_issue" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>File Number</label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" placeholder="File Number" value="" id="txt_file_no" name="6_file_no" maxlength="12">
                                    <span id="error_txt_file_no" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<br>
        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>7. Documents Submitted as Proof</h4>
                        <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-holder">
                                <label for="male">Address Proof
                                    <!--span style="color:red">*</span-->
                                </label>
                                <select class="form-control" name="8_document_proof" id="contact_8_document_proof" >

                                    <option value="">Select</option>
                                    <option value="Aadhaar Card">Aadhaar Card</option>
                                    <option value="Voter ID">Voter ID</option>
                                    <option value="Electricity Bill">Electricity Bill</option>
                                    <option value="Telephone Bill">Telephone Bill</option>
                                    <option value="Water Bill">Water Bill</option>
                                    <option value="Parents Passport">Parents Passport</option>
                                    <option value="Rent Agreement">Rent Agreement</option>
                                    <option value="Bank Account Passbook">Bank Account Passbook</option>
                                    <option value="Gas Connection Bill">Gas Connection Bill</option>
                                    <option value="IT Assessment Order">IT Assessment Order</option>
                                    <option value="Employer Certificate">Employer Certificate</option>
                                    <option value="Spouse Passport">Spouse Passport</option>

                                </select>
                                <div id="error_proof" style="color:red"></div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-holder">
                                <label for="male">Date of Birth Proof
                                    <!--span style="color:red">*</span!-->
                                </label>
                                <select class="form-control" name="8_dob_proof" id="contact_8_dob_proof" >

                                    <option value="">Select</option>
                                    <option value="Pan Card">Pan Card</option>
                                    <option value="Aadhar Card">Aadhar Card</option>
                                    <option value="Driving Licence">Driving Licence</option>
                                    <option value="Voter ID">Voter ID</option>
                                    <option value="Birth Certificate">Birth Certificate</option>
                                    <option value="Transfer/School Leaving Certificate">Transfer/School Leaving Certificate</option>
                                    <option value="Matriculation/10th/12th Certificate">Matriculation/10th/12th Certificate</option>
                                    <option value="Service Record/Pay Pension Order">Service Record/Pay Pension Order</option>
                                    <option value="Policy Bond">Policy Bond</option>
                                    <option value="Orphan Declaration">Orphan Declaration</option>

                                </select>
                                <div id="error_proof83" style="color:red"></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="" style="text-align: center; margin-top: 20px; margin-bottom:20px;">
            <button class="btn btn-warning" value="submit" type="submit">Submit & Pay</button>  
        </div>
</form>


        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Are you eligible for Non-ECR category?</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #ffffff;">
                        <p style="color:red">Select Yes, if you fall in the following Category: </p>
                        <p>1. All persons having educational qualifications of 10th and above.</p>
                        <p>2. All holders of Diplomatic/official Passports.</p>
                        <p>3. All GAZETTED Government servants.</p>
                        <p>4. All Income-Tax payers (including Agricultural Income-Tax Payees) in their individual capacity.</p>
                        <p>5. Spouses and dependent children of category of persons listed from (2) to (4).</p>
                        <p>6. Seamen who are in possession of CDC or Sea Cadets, Deck Cadets;</p>
                        <p>7. Persons holding Permanent Immigration Visa, such as the visas of UK, USA and Australia.</p>
                        <p>8. Nurses possessing qualifications recognized under the Indian Nursing Council Act. 1947.</p>
                        <p>9.All persons above the age of 50 years. </p>
                        <p>10. All persons who have been staying abroad for more than three years and their spouses. </p>
                        <p>11. All children up to the age of 18 years of age.</p>
                        <p style="color:red">Select No, if you do not fall in any of the above category.</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal2" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Visible Distinguishing Mark (If any?) </h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #ffffff;">

                        <p>Visible Distinguishing mark means any birthmark, mole, scare etc. on your body that is easily seen and could be used to confirm identity.</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="modal fade" id="myModal3" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #ff6613;">Have you ever held/hold any Identity Certificate?</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="background-color: #ffffff;">
                        <p>
                            Identity Certificate(IC) is normally issued to Tibetan/other stateless people residing in India</p>

                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!--Footer-->
 @include('includes.footer')

    <!--contact ends-->
    <style>
        a {
            padding-left: 10px;
        }
    </style>

    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/custom.js"></script>
    <script src="contactform/contactform.js"></script>

    <script src="js/form_validate.js"></script>

</body>
</html>