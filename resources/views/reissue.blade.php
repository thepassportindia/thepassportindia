@extends('layouts.master')

@section('content')

<!-- Page Content Holder -->
<div id="content" class="col-xs-12">

    <div class="row" class="col-xs-12">
        <div class="col-xs-12">
            <h1 class="display-3">Contacts</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Uniq ID</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Company</td>
                        <td>Phone No</td>
                        <td>Gender</td>
                        <td colspan=2>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $contact)
                        @if($contact->applying == 'Reissue Passport')
                    <tr>
                        <td>{{$contact->id}}</td>
                        <td>{{$contact->uniq_id}}</td>
                        <td>{{$contact->name}}</td>
                        <td>{{$contact->email}}</td>
                        <td>{{$contact->company}}</td>
                        <td>{{$contact->phone}}</td>
                        <td>{{$contact->gender}}</td>
                        <td>
                            <a href="{{ route('view', $contact->id) }}" class="btn btn-primary" target="_blank">View</a> {{ csrf_field() }}
                        </td>
                    </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $data->links() }}
</div>   
@endsection