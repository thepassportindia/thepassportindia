 <!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="assets/img/favicon.ico" type="image/gif">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<!-- daterange picker -->
<link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">

  <title>ONLINEPASSPORT Registration</title>
  <style type="text/css">

  </style>
</head>

<body onload="" id="form1">
    <div class="header " id="header" >
      <!-- header-images -->
      <div class="container mb-10px">
        <div class="row ">
          <div class="col-md-4 mt-10px" >
            <!-- <a href="index-2.html"><img style="max-width: 328px; max-height: 100%;" src="assets/img/PassportLogo.png"></a> -->
            <h2>Indian Consultancy</h2>
          </div>

          <div class="col-md-5 d-none d-md-block mt-10px" >
            <a href="index-2.html"><img  style="width:100%; max-width: 350px;" src="assets/img/PassportCenterLogo_new.png"></a>
          </div>

          <div class="col-md-1 clearfix d-none d-md-block" style="margin-left: 0px;">
            <img class="mt-10px float-left mt-20px" style="width: 100%;min-width: 80px;" src="assets/img/PassportPich.png">
          </div>
          <div class="col-md-2 d-none d-md-block">
            <button class="btn btn-primary mt-10px" href="javascript:void(0)" data-toggle="modal" data-target="#myModalenq" style="width: 100%; background-color: #ed7f1a!important; border-color: #ed7f1a!important;">Enquire Now</button>
            <button onclick="location.href = 'contact.html';" class="btn btn-primary mt-10px" style="width: 100%; background-color: #0f172e; border-color: #0f172e;">Contact Us</button>
           <!-- <img class="header-right-image" src="assets/img/shopact-msme-gst-swacha-bharat.png">-->
          </div>
        </div>
      </div>

      <!-- nav-bar -->
      <nav class="navbar navbar-expand-lg navbar-light bg-color-golden p-01rem fs-14">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse justify-content-md-center collapse text-white" id="navbarsExample08" style="">
          <ul class="navbar-nav">
            <li class="nav-item dropdown active">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                APPLY NOW
              </a>
<!--               <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="new-passport.html">NEW PASSPORT</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="reissue-passport.html">REISSUE PASSPORT</a>
              </div> -->
            </li>

            <!-- <li class="nav-item active">
              <a class="nav-link plr-15rem " href="index">APPLY NOW</a>
            </li> -->
             <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="https://thepassportindia.org/process">PROCEDURE</a>
            </li> 
            <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="https://thepassportindia.org/documents">REQUIRED DOCUMENTS</a>
            </li>
<!--             <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="modify.html">MODIFY APPLICATION</a>
            </li>
 -->
             <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="fees-structure.html">FEES</a>
            </li>
             
            <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="https://thepassportindia.org/faq">FAQ</a>
            </li>
<!--            <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="track-order.html">TRACK</a>
            </li> -->
                   
            
<!--             <li class="nav-item active">
              <a class="nav-link plr-15rem menu-color-green" href="police-station.html">FIND YOUR POLICE STATION</a>
            </li> -->
            <li class="nav-item active d-sm-none">
              <a class="nav-link plr-15rem bg-color-golden" href="https://thepassportindia.org/contact">CONTACT US</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid bg-color-golden-light ptb-1rem text-center" style="background-color: #ee7f1a !important;">
        <marquee class="text-white d-none d-md-block"><b>Apply Online For PASSPORT Application - Easy & Simple Process (Private Consultancy Service Provider)</b></marquee>
        <p class="text-black fs-14 d-sm-none"><b  style="font-size: 20px;"> Private Consultancy Service Provider </b></p>

      </div>

<!--         <div class="container-fluid bg-color-golden-light ptb-1rem text-center" style="background-color: green !important;">
        <marquee class="text-white d-none d-md-block"><b>This website is not in any manner related to <b>Ministry of External Affairs/ Passport Department of India/ any other Government Authority in India</b></marquee>
       </div> -->
    
    </div>
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
  <script src="../ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <link rel="stylesheet" href="../cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css">
    .blinking {
      animation: blinkingText 0.5s infinite;
    }

    @keyframes blinkingText {

      0% {
        color: transparent;
      }

      0% {
        color: transparent;
      }

      0% {
        color: transparent;
      }

      100% {
        color: #000;
      }
    }
  </style>

  <div class="main-content" id="main-content">
    <div class="container-fluid" style="background-image: url(assets/img/bg1.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mt-50px mb-50px">
            <div class="col-md-12">
              <h2 class="text-white">Online Passport Consultancy Services</h1>
                <!-- <h4 class="text-white">Get started now @ just Rs. 2500/- (Excluding Govt. Fees)</h4> -->
              </h2>
              <h5 class="text-white">Package Inclusive of :</h3>
              <div class="col-md-6 text-white">
                <ul>
                  <li>Application Drafting & Processing</li>
                  <li>Application Filing</li>
                  <li>Book a Date of Appointment</li>
                  <li>Get Passport via Post</li>
                  <li>Proper Consultancy</li>
                  <li>Dedicated Support Team</li>
                </ul>
              </div>
            </div>
                 <div class="col-md-12">
                <div class="col-md-4">
                <a href="https://thepassportindia.org/passport" id="submit-btn1" class="btn mb-2 ptxb-0505 h-32px text-white w-100p" style="background-color: #ed7f1a!important">Apply New Passport</a>
                </div>
                <div class="col-md-4">
                  <a href="https://thepassportindia.org/passport" id="submit-btn1" class="btn mb-2 ptxb-0505 h-32px text-white w-100p" style="background-color: #ed7f1a!important">Apply Re-issue Passport</a>
                  </div>
            </div>
            <div class="col-md-12">
            <p class="text-white"><b>Note:</b> onlinepassportconsultancy.com is owned and operated by a consultancy firm and in no way represent any relation to any government body or Passport Seva.</p> 
            </div>
          </div>
          <div class="col-md-4 mt-50px mb-50px">
            <div class="col-md-12 box-shadow-form mt-10px mb-10px" style="background: white;">
            <form id="passport-enquiry-form1" name="passport_enquiry_form">
                <div class="form-group mb-0 row justify-content-center">
                  <div class="col-sm-12 mt-10px text-center">
                    <label class="control-form-label fw-700 fs-20 mb-0 pt-0">Enquire Now</label>
                  </div>
                  <div class="col-sm-12 mt-20px">
                    <input type="text" class="form-control ptxb-0505 h-32px" id="enquiry-full-name" name="enquiry_full_name" placeholder="Full Name" required>
                  </div>
                  <div class="col-sm-12 mt-20px">
                    <input type="email" class="form-control ptxb-0505 h-32px" id="enquiry-email-id" name="enquiry_email_id" placeholder="Email ID" required>
                  </div>
                  <div class="col-sm-12 mt-20px">
                    <input type="text" pattern="[789]{1}[0-9]{9}" maxlength="10" class="form-control ptxb-0505 h-32px" id="enquiry-mobile-no" name="enquiry_mobile_no" placeholder="Mobile No." required>
                  </div>
                  <div class="col-sm-12 mt-20px">
                    <select class="form-control  ptxb-0505 h-32px" name="enq_type" id="enq_type" required>
                      <option value="">Select Enquiry Type</option>
                      <option value="passport-enquiry-form">New Passport </option>
                      <option value="reissue-passport-enquiry-form">Reissue Passport </option>
                    </select>
                  </div>
                  <div class="col-sm-12 mt-20px mb-20px text-center">
                    <button type="submit" id="submit-btn1" class="btn mb-2 ptxb-0505 h-32px text-white w-100p" style="background-color: #ed7f1a!important">SUBMIT</button>
                    <svg class="lds-microsoft dn" id="submit-loader1" width="32px" height="32px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;">
                      <g transform="rotate(0)">
                        <circle cx="73.801" cy="68.263" fill="#f05125" r="2" transform="rotate(124.659 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="0s"></animateTransform>
                        </circle>
                        <circle cx="68.263" cy="73.801" fill="#fdb813" r="2" transform="rotate(146.962 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.062s"></animateTransform>
                        </circle>
                        <circle cx="61.481" cy="77.716" fill="#7fbb42" r="2" transform="rotate(170.533 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.125s"></animateTransform>
                        </circle>
                        <circle cx="53.916" cy="79.743" fill="#32a0da" r="2" transform="rotate(193.998 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.187s"></animateTransform>
                        </circle>
                        <circle cx="46.084" cy="79.743" fill="#f05125" r="2" transform="rotate(217.442 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.25s"></animateTransform>
                        </circle>
                        <circle cx="38.519" cy="77.716" fill="#fdb813" r="2" transform="rotate(239.508 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.312s"></animateTransform>
                        </circle>
                        <circle cx="31.737" cy="73.801" fill="#7fbb42" r="2" transform="rotate(260.384 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.375s"></animateTransform>
                        </circle>
                        <circle cx="26.199" cy="68.263" fill="#32a0da" r="2" transform="rotate(279.035 50 50)">
                          <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.437s"></animateTransform>
                        </circle>
                        <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;0 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s"></animateTransform>
                      </g>
                    </svg>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-6 mt-50px mb-50px">
          <h2 style="color: #ed7f1a!important">PASSPORT MEANS</h2><hr class="hr f-left"/><br/>
          <p>Passports are small booklets that typically contain the bearer’s name, place of birth, date of birth, the date of issue, date of expiry, passport number, photo and signature.</p>
          <p>A passport is a travel document, usually issued by a country's government, which certifies the identity and nationality of its holder primarily for the purpose of international travel.</p>
          <p>A passport is used to verify one's country of citizenship. If traveling outside your country, it is used to regain entry into your country of citizenship.</p>
        </div>
        <div class="col-md-6 mt-50px mb-50px">
          <img src="assets/new/Passport_Image_1.jpg" alt="" style="width: 100%; height: auto;">
        </div>
      </div>
    </div>

<!--     <div class="container-fluid bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mt-50px mb-50px">
            <img src="assets/new/Passport_Image_2.jpg" alt="" style="width: 100%; height: auto;">
          </div>
          <div class="col-md-6 mt-50px mb-50px">
            <h2  style="color: #ed7f1a!important">TYPES OF PASSPORT</h2><hr class="hr f-left" /><br/>
            <p style="text-align: justify;">There are three types of Indian Passport, which are: <br>
            1) Regular Passport has a Navy Blue cover and is issued for ordinary travel, such as vacations and business trips. <br>2) Diplomatic Passport has a Maroon cover and is issued to Indian diplomats, top ranking government officials and diplomatic couriers.<br>3)  Official Passport has a White cover and it is issued to individuals representing the Indian government on official business.</p>
          </div>
        </div>
      </div>
    </div> -->

<!--     <div class="container">
      <div class="row">
        <div class="col-md-6 mt-50px mb-50px">
          <h2 style="color: #ed7f1a!important">VALUE OF AN INDIAN PASSPORT</h2><hr class="hr f-left"/><br/>
          <p style="text-align: justify;">An Indian passport is a passport issued by order of the President of India to Indian citizens for the purpose of international travel. It enables the bearer to travel internationally and serves as proof of Indian citizenship as per the Passports Act (1967).</p>
          <ul>
            <li>A passport is the main travel document to travel outside India.</li>
            <li>Visa is a granted and stamped in the passport booklet to stay in a foreign country for a period of time.</li>
            <li>International travel details of the passport holder can be found in their passport.</li>
            <li>Foreign Travel.</li>
            <li>Indian passport holders in 2018 were able to travel 25 countries without a visa.</li>
            <li>Indian Passport is 71 most Powerful Passport in the World.</li>
            <li>Indian Passport is valid for 10 years.</li>
            <li>Passport can also be used as a residence or identity proof.</li>
          </ul>
        </div>
        <div class="col-md-6 mt-50px mb-50px">
          <img src="assets/new/Passport_Image_3.jpg" alt="" style="width: 100%; height: auto;">
        </div>
      </div>
    </div> -->


    <div class="container-fluid bg-gray">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mt-50px mb-20px">
            <h3 style="color: #ed7f1a!important">Why to Choose Us</h3><hr class="hr"/>
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/1_Choose_the_Service.png" alt="" style="width: 75%; height: auto;">
            <br>
            Get A Call Back From Expert
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/2_Fill_the_Application_Form.png" alt="" style="width: 75%; height: auto;">
            <br>
            All India Online Service
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/3_Make_Online_Payment.png" alt="" style="width: 75%; height: auto;">
            <br>
            Free Expert Counselling
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/4_Executive_will_Process_the_Form.png" alt="" style="width: 75%; height: auto;">
            <br>
            All PASSPORT Services At One-Stop
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/5_Executive_Book_the_Appointment.png" alt="" style="width: 75%; height: auto;">
            <br>
            Filing In Same Day
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/6_Get_Passport.png" alt="" style="width: 75%; height: auto;">
            <br>
            Cashback Guarantee
          </div>
        </div>
      </div>
    </div>


<!--     <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center mt-50px mb-20px">
            <h3 style="color: #ed7f1a!important">Procedure to obtain Passport</h3><hr class="hr "/>
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/1.png" alt="" style="width: 75%; height: auto;">
            <br>
            Choose Required Service
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/2.png" alt="" style="width: 75%; height: auto;">
            <br>
            Fill Up Application Form
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/4.png" alt="" style="width: 75%; height: auto;">
            <br>
            Make Online Payment
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/3.png" alt="" style="width: 75%; height: auto;">
            <br>
            Executive Will Process Application
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/5.png" alt="" style="width: 75%; height: auto;">
            <br>
            Executive Will Book a Date of Appointment
          </div>
          <div class="col-md-2 mt-20px mb-50px text-center">
            <img src="assets/new/6.png" alt="" style="width: 75%; height: auto;">
            <br>
            Get Passport Via Post
          </div>
        </div>
      </div>
    </div> -->


 <!-- Modal3 for open onpage load -->
 <div class="modal fade" id="myModal3" role="dialog" data-keyboard="false" data-backdrop="static" style="top: 120px !important">
    <div class="modal-dialog modal-800">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title model_title"><b>LEGAL DISCLAIMER</b></h5>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          
        </div>
        <div class="modal-body disclaimer">
        <p class="justify">OnlinePassportConsultancy is a non-government entity and is a private consultancy in nature and NOT ASSOCIATED OR RELATED in any way with the MINISTRY OF EXTERNAL AFFAIRS, PASSPORT SEVA PROJECT nor with any GOVERNMENT WEBSITE meant for application, issuance or renewal of passports for Indian Citizens.</p>
        <p class="justify">
          WE DO NOT ADVERTISE, SOLICIT NOR INVITE ANY PERSON TO AVAIL SERVICES FROM US. Prospective applicants for passport services suo moto visit our website for the consultancy and processing services. In addition, OnlinePassportConsultancy under any situation DOES NOT PROMISE the service from any government agency which be kindly noted before utilizing our services.</p>
          <p class="justify">
          We charge for our consultancy services only and do not charge or collect funds in the name of the Government of India or any other related Government Agency including Passport Services.
          We apply on your behalf and help you with the processing stages and the moment you press the ‘I Agree’ button on our website to accept the ‘Terms of Acceptance’, you authorize us to proceed on your behalf.  
          </p>
         <p class="justify"><span style="color:red"><u>Privacy Policy</u>:</span><br> <p class="justify">Thank you very much for visiting our website and your interest shown in our onlinepassportconsultancy.com, an Online Passport Application Private Consultancy Service Provider.</p>
         <p class="justify">We highly value your trust and will strive to protect the security and privacy of any personal information you provide us with.</p>
         <p class="justify">The privacy and security of your personal information is very important to us. We do not share your personal information in ways not disclosed in our privacy statement or without your consent.</p>
         <p class="justify">This document outlines the type of personal data we receive and collect when you use our website, as well as some of the precautions we take to safeguard the information. onlinepassportconsultancy.com aspires to design and practice all business processes according to the data regulations of respective country.</p>
         <p class="justify">If you have any questions regarding the data privacy statement or privacy concerns, you may contact us through our website.</p></p>
         
          <p class="justify"><span style="color:red"><u>Not associated with Ministry of External Affairs (MEA)</u>:</span><br> 
         <p class="justify">It is informed to the user that the said website is only for providing consultancy services to help prospective customer apply for their passport, not associated with Ministry of External Affairs (MEA) and they shall provide their personal data in acceptance of the said fact and their relationship with the website shall be governed by the terms & conditions, privacy policy, refund policy and disclaimer displayed on the website.</p></p>

          <p class="justify"><span style="color:red"><u>Collection and processing of personal data</u>:</span><br> 
         <p class="justify">You may visit our website without disclosing who you are. Information about usage patterns such as the time and date of the usage, the amount of data sent, browser, referral domain, the geographic location of the user and the name of the files sent are collected for the purpose of statistical evaluation. We will use this information for the purpose of evaluating your use of the website, compiling reports on website activity and providing other services relating to website activity and internet usage. This data does not enable us to identify you, and we use it in an anonymous way. We do not collect or store the IP addresses.</p></p>

          <p class="justify"><span style="color:red"><u>Collection and processing of personal data</u>:</span><br> 
         <p class="justify">You may visit our website without disclosing who you are. Information about usage patterns such as the time and date of the usage, the amount of data sent, browser, referral domain, the geographic location of the user and the name of the files sent are collected for the purpose of statistical evaluation. We will use this information for the purpose of evaluating your use of the website, compiling reports on website activity and providing other services relating to website activity and internet usage. This data does not enable us to identify you, and we use it in an anonymous way. We do not collect or store the IP addresses.</p>
         <p class="justify">We collect personal information, when you choose to provide it voluntarily. Usually this occurs when a business relationship is established in the context of a Passport Application request and to reserve the appointment for Passport Application in India.</p>
         <p class="justify">
         Personal information encompasses personal details, which are entered, when you place a request such as your name, email address, mailing address and phone number etc. Additional information is required for some activities such as New Passport Service, Tatkal Passport Service, Reissue Passport Service to process the Application. onlinepassportconsultancy.com management is committed to ensure that your information is secure. We have put in place suitable physical, electronic and managerial procedures in order to prevent unauthorized access or disclosure to safeguard and secure the information we collect online. This personal information is transmitted only in a secure and encrypted manner.</p></p>

          <p class="justify"><span style="color:red"><u>Use and processing of data</u>:</span><br> 
         <p class="justify">In case you choose to use services of onlinepassportconsultancy.com, we collect data that is necessary for the service you requested. If we ask you for more information, it will be on voluntary basis. We use this information to process and fulfill your service request, i.e. a New Passport Application, Tatkal Passport Application, Reissue Passport Application etc.</p>

         <p class="justify">The deletion of personal data on our website happens, if you revoke your agreement to store it or if it is no longer necessary for us to store to it to fulfill your service request or if the storage is not permitted by law.</p>
         <p class="justify">We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law.</p></p>

         <p class="justify"><span style="color:red"><u>Personal data transmitted to 3rd parties</u>:</span><br> 
         <p class="justify">We use and process your data in case you choose to use services of onlinepassportconsultancy.com, data will be transmitted in order to fulfill your service request, i.e. data with the details of the New Passport Application, Tatkal Passport Application, Reissue Passport Application etc. will be transmitted to the requested service departments.</p>

         <p class="justify">We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law.</p></p>

          <p class="justify"><span style="color:red"><u>Use of cookies</u>:</span><br> 
         <p class="justify">A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or indicates when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. Cookies do not damage your computer, neither do cookies contain viruses. </p>

         <p class="justify">You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. You can also delete stored cookies in your browser setting. If you refuse the use of cookies this may prevent you from taking full advantage of the website.</p></p>

         <p class="justify"><span style="color:red"><u>Security</u>:</span><br> 
         <p class="justify">If you choose to provide personal data, such as your name, email address, mailing address and phone number also additional information which is required during the New Passport Application, Tatkal Passport Application, Reissue Passport Application etc., this information will be transferred in an encrypted manner. onlinepassportconsultancy.com website and servers have security measures in place to help protect your personal identifiable information against loss, misuse, and alteration while under our control. </p>

         <p class="justify">onlinepassportconsultancy.com management recognizes the importance of information security and is constantly reviewing and enhancing our technical, physical, and logical security rules and procedures.</p></p>

         <p class="justify"><span style="color:red"><u>Minors</u>:</span><br> 
         <p class="justify" >onlinepassportconsultancy.com management does not seek to obtain nor does it wish to receive personal identifiable information directly from minors; however, we cannot always determine the age of persons who access and use our websites. onlinepassportconsultancy.com will not make use of the data if a minor (as defined by applicable law) provides us with his/her data without parental or guardian consent. </p></p>

    

         <p class="justify"><u><b>GRIEVANCE REDRESSAL PORTAL FOR onlinepassportconsultancy.com</b></u>:</p>

         <p class="justify"><span style="color:red"><u>About this Portal</u>:</span><br> 
         <p class="justify">The Portal has been designed for lodging complaints by user. They can lodge Complaint here indicating issues or problems faced by them while working on our website instead of sending emails to the support. It has been designed in a manner that the user can explain issues faced and upload screenshots of pages where they faced the problem, for quick redressal of grievances.</p></p>

         <p class="justify"><u>Having this portal has the following advantages</u>:<br> 
         <p class="justify"><ul>
             <li class="justify">  Enable the user to lodge his complaint and raise tickets himself.</li>
             <li class="justify">  To provide all required information and reducing to and from communication between helpdesk and the user, helping to reach a faster resolution.</li>
             <li class="justify">  Enable the user to check the progress of resolution of his complaint by using the ticket number (acknowledgement number generated after a complaint is lodged).</li>
             <li class="justify">  Check the resolution comments in case the complaint/ticket is closed.</li>
         </ul></p></p> 

          <p class="justify"><span style="color:red"><u>Acceptance of terms</u>:</span><br> 
         <p class="justify">By accessing and/or using the onlinepassportconsultancy.com website, you accept and agree to be bound by this policy. All content available on the onlinepassportconsultancy.com website is subject to this policy. If you do not agreed to this policy, you may not use, access or submit information to the website..</p>
         <p class="justify">Get in touch for any clarifications</p>

         <p class="justify">If you have any questions or concerns about this policy, please get in touch with us at <a href="mailto:support@onlinepassportconsultancy.com" target="_top">support@onlinepassportconsultancy.com</a></p>
        </p>
         
        </div>
        <div class="modal-footer agree">
          <button type="button" class="btn btn-success" data-dismiss="modal">I understand, accept, and agree to the following terms and conditions</button>
        </div>
      </div>
      
    </div>
  </div>


  </div>
  <script>
  window.addEventListener('load',function(){
    var set_timer = setInterval(function(){
      if(jQuery('#passport-enquiry-form-thank-you:contains("Thank You")').is(':visible')){
        gtag('event', 'conversion', {'send_to': 'AW-799503904/fbklCPrsq6cBEKDsnf0C'});
        clearInterval(set_timer);
      }
    },900)
    })
</script>

 @include('includes.footer')

<!-- Enquiry Modal -->
<div class="modal fade" id="myModalenq">
  <div class="modal-dialog">
    <div class="modal-content">
    
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title text-orange">Enquiry Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      
      <!--<form id="pan-enquiry-form" name="pan_enquiry_form">-->
        <form id="passport-enquiry-form" name="passport_enquiry_form">
        <!-- Modal body -->
        <div class="modal-body" id="pan-enquiry-form-field">
          <div class="form-group">
            <input type="text" class="form-control ptxb-0505 h-32px" id="enquiry-full-name" name="enquiry_full_name" placeholder="Full Name" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control ptxb-0505 h-32px" id="enquiry-email-id" name="enquiry_email_id" placeholder="Email ID" required>
          </div>
          <div class="form-group">
            <input type="text" pattern="[789]{1}[0-9]{9}" maxlength="10" class="form-control ptxb-0505 h-32px" id="enquiry-mobile-no" name="enquiry_mobile_no" placeholder="Mobile No." required>
          </div>
          <div class="form-group mb-0">
            <select class="form-control" name="enq_type" id="enq_type" required>
              <option value="">Select Enquiry Type</option>
              <option value="passport-enquiry-form">New Passport </option>
              <option value="reissue-passport-enquiry-form">Reissue Passport </option>
             
            </select>
          </div>
        </div>

        
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" id="submit-btn" class="btn bg-black mb-2 ptxb-0505 h-32px text-white w-100p">SUBMIT</button>
          <svg class="lds-microsoft dn" id="submit-loader" width="32px"  height="32px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;">
                <g transform="rotate(0)"><circle cx="73.801" cy="68.263" fill="#f05125" r="2" transform="rotate(124.659 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="0s"></animateTransform>
                  </circle><circle cx="68.263" cy="73.801" fill="#fdb813" r="2" transform="rotate(146.962 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.062s"></animateTransform>
                  </circle><circle cx="61.481" cy="77.716" fill="#7fbb42" r="2" transform="rotate(170.533 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.125s"></animateTransform>
                  </circle><circle cx="53.916" cy="79.743" fill="#32a0da" r="2" transform="rotate(193.998 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.187s"></animateTransform>
                  </circle><circle cx="46.084" cy="79.743" fill="#f05125" r="2" transform="rotate(217.442 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.25s"></animateTransform>
                  </circle><circle cx="38.519" cy="77.716" fill="#fdb813" r="2" transform="rotate(239.508 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.312s"></animateTransform>
                  </circle><circle cx="31.737" cy="73.801" fill="#7fbb42" r="2" transform="rotate(260.384 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.375s"></animateTransform>
                  </circle><circle cx="26.199" cy="68.263" fill="#32a0da" r="2" transform="rotate(279.035 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s" begin="-0.437s"></animateTransform>
                  </circle><animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;0 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="1.9s"></animateTransform>
                </g>
          </svg>
          <button type="button" class="btn btn-danger mb-2 ptxb-0505 h-32px" data-dismiss="modal">Close</button>
        </div>
      </form>
      <div class="modal-body dn" id="passport-enquiry-form-thank-you">
          <div class="col-md-12 text-center color-green">
            <h3 class="pt-7px">Thank You, We will get you back soon.</h3>
          </div>
          <hr/>
          <button type="button" style="float: right;" class="btn btn-danger mb-2 ptxb-0505 h-32px" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>  <!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="../code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="../cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- date-range-picker -->
<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="../checkout.razorpay.com/v1/checkout.js"></script>
<script src="assets/js/script.js"></script>  <script src="assets/js/passport_frm.js"></script>

  <!------ End of exit pop up ---------->
  <!-- Script to Activate the Carousel -->

  <script type="text/javascript">
     $(window).load(function(){        
       $('#myModal3').modal('show');
        }); 
    $(document).ready(function() {
      $('#myModal3').modal('show');
   
      if (sessionStorage.getItem('advertOnce') !== 'true') {
           $('#myModal3').modal('show');
           sessionStorage.setItem('advertOnce','true');
       }


    });
  </script>

  <script>
    $("#sel_emp_type_old").change(function() {
      var emp_type = $(this).val(); // get selected value
      $("#sel_emp_type").tooltip('dispose');
      var t1 = "Students staying away from their parents have the option of applying for a passport from their place of study. In such cases, a bonafide certificate from the Principle/ Director/ Registrar/ Dean of the educational institution (On official letter head of UGC recognized College) is required to be submitted as proof of address.";
      var t2 = "Please furnish the No Objection Certificate (NOC) as per Annexure 'G' OR Prior Intimation Letter (PIL) OR Identity Certificate (IC) in original as per Annexure 'A' for more details please refer 'Document Advisor' Section.";
      var t3 = "";
      if (emp_type == 'Student') {
        $('#sel_emp_type').tooltip({
          'trigger': 'click',
          'title': t1
        });
      } else if (emp_type == 'Government' || emp_type == 'PSU' || emp_type == 'Statutory Body') {
        $('#sel_emp_type').tooltip({
          'trigger': 'click',
          'title': t2
        });
      } else {
        $('#sel_emp_type').tooltip({
          'trigger': 'click',
          'title': t3
        });
      }

    });



    var serverpath = "#";
    //var serverpath = "https://www.passportonlineindia.com/"; 
    function change_form(frm) {
      //alert(frm.value);
      switch (frm.value) {
        case 'New':
          window.location.assign(serverpath);
          break;
        case 'Reissue':
          window.location.assign(serverpath + 'reissue-passport');
          break;
        default:
          window.location.assign(serverpath);
      }
    }

    function validate_top_contact_form() {
      var errorflag = true;
      /* remove errorflag */
      removeErrorMessage('txt_top_name');
      removeErrorMessage('txt_top_email');
      removeErrorMessage('txt_top_phone');

      if (check_empty('txt_top_name')) {
        setErrorMessage('txt_top_name', "Please enter applicant name");
        errorflag = false;
      } else {
        if (!isAlphabet('txt_top_name')) {
          setErrorMessage('txt_top_name', "Only alphabets are allowed");
          errorflag = false;
        }
      }
      if (check_empty('txt_top_email')) {
        setErrorMessage('txt_top_email', "Please enter mail ID");
        errorflag = false;
      } else {
        if (!emailValidator('txt_top_email')) {
          setErrorMessage('txt_top_email', "Please enter valid mail ID");
          errorflag = false;
        }
      }

      if (check_empty('txt_top_phone')) {
        setErrorMessage('txt_top_phone', "Please enter mobile number");
        errorflag = false;
      } else {
        if (!isphone('txt_top_phone')) {
          setErrorMessage('txt_top_phone', "Please enter valid mobile number");
          errorflag = false;
        } else {
          if (!lengthRestriction('txt_top_phone', 10, 10)) {
            setErrorMessage('txt_top_phone', "Mobile number must be 10 digit");
            errorflag = false;
          }
        }
      }
      return errorflag;
    }

    function toggleIcon(e) {
      $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);

    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;

      return true;
    }
    //--==================Drop Down Menu JS ====================   

    $(function() {
      //$('#form1 input[type="text"]').prop("disabled", true);
      $('#form1 input[type="text"]').keyup(function() {
        var yourInput = $(this).val();
        var res = yourInput.charAt(0)
        re = /[ `~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        var isSplChar = re.test(res);
        if (isSplChar) {
          var no_spl_char = yourInput.replace(/[ `~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
          $(this).val(no_spl_char);
        }
      });


      $(".dropdown").hover(
        function() {
          $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
          $(this).toggleClass('open');
          $('b', this).toggleClass("caret caret-up");
        },
        function() {
          $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
          $(this).toggleClass('open');
          $('b', this).toggleClass("caret caret-up");
        });
    });

    $('#txt_lastname').keyup(function() {
      if ($.trim($('#txt_lastname').val()) != '') {
        $('#txt_panlname').val(this.value);
      }
    });
  </script>
<script type="text/javascript">
	$(window).load(function(){        
   $('#myModal3').modal('show');
    }); 
 $(document).ready(function () {
$('#myModal3').modal('show');

});
</script>
</body>


<!-- Mirrored from onlinepassportconsultancy.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Dec 2019 15:12:25 GMT -->
</html>