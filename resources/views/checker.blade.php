@section('main')

@include('includes.head')

    <style>
        h1 {
            text-align: center;
            color: #000;
        }
        
        h5 {
            margin-top: 30px;
            text-align: center;
        }
        
        marquee {
            background-color: #5DA6D3;
            font-size: 16px;
            padding-top: 5px;
            padding-bottom: 5px;
            color: #000;
            font-weight: 700;
            letter-spacing: 3px;
        }
        
        .img-11 {
            display: none;
        }
        
        .img-21 {
            display: none;
        }
        
        .img-2>h3 {
            text-align: center;
            font-size: 30px;
            margin-bottom: -20px;
        }
        
        .section-head {
            background-color: #5DA6D3;
            width: 100%;
            #border-radius: 10px;
            margin-bottom: 15px;
        }
        
        .card {
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            #border: 2px solid #686868;
            #border-radius: 15px;
            -webkit-box-shadow: 10px 10px 15px 2px #686868;
        }
        
        h2 {
            color: #1dc8cd;
        }
        
        h4 {
            color: white;
        }
        
        .enq {
            padding-left: 80px;
            color: #ffffff;
            font-size: 20px;
        }
        
        .enq-bg {
            #background: #ff8923;
            background: #2e5fa7;
            padding: 10px 10px 10px 10px;
        }
        
        .marq-blink {
            #animation: blinkingText 1s infinite;
        }
        
        @keyframes blinkingText {
            0% {
                color: #000;
            }
            #49% {
                color: transparent;
            }
            #50% {
                color: transparent;
            }
            #99% {
                color: transparent;
            }
            100% {
                color: #f28727;
            }
        }
        
        p {
            color: black;
        }
        
        .error {
            color: red;
        }
        
        .navbar-default {
            margin-top: 140px;
        }
        
        .top-logo {
            background-color: #ffffff;
            #margin-left: 20px;
            padding-left: 30px;
            margin-right: -10px;
            background: cover;
        }
        
        .faq-head {
            background: gray;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .btn-warning {
            background: #3e70cb;
        }
        
        @media only screen and (max-width: 600px) {
            .h4,
            .h5,
            .h6,
            h4,
            h5,
            h6 {
                margin-top: 0px;
                margin-bottom: 10px;
                padding-top: 5px;
                padding-bottom: 5px;
            }
            .main-header {
                min-height: 110px;
            }
            .img-1 {
                display: none;
                height: 10px;
                width: 10px;
            }
            .img-2 {
                display: none;
                #font: 10px;
                #text-align: right;
                #padding-left: 10px;
            }
            .img-21 {
                display: block;
            }
            .img-21>h3 {
                font-size: 16px;
                text-align: left;
            }
            .img-2 h3 {
                font-size: 20px;
                text-align: right;
                margin-top: 10px;
            }
            .img-3 {
                display: none;
            }
            .img-11 {
                display: none;
                #height: 15px;
                #width: 15px;
                #margin-left: 5px;
            }
            .top-nav-collapse {
                #padding: 25px 0;
            }
            .navbar-default {
                margin-top: 0px;
            }
            .form-control {
                margin-top: 10px;
            }
            h1 {
                font-size: 25px;
            }
        }
    </style>

    <style>
        .error {
            display: none;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid {
            border: 2px solid green;
        }
    </style>
</head>

<body>

    <link rel="stylesheet" href="../code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="../code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
    <script src="../code.jquery.com/jquery-1.12.4.js"></script>
    <script src="../code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="newapply.js"></script>

    <style>
        .error {
            display: none;
            //margin-left: 10px;
        }
        
        .error_show {
            color: red;
            margin-left: 10px;
        }
        
        input.invalid,
        textarea.invalid,
        select.invalid {
            border: 2px solid red;
        }
        
        input.valid,
        textarea.valid,
        select.valid {
            border: 2px solid green;
        }
    </style>
    <script>
        $("#myform").validate({
            ignore: ".ignore"
        });
    </script>


        <div id="myform">
            <section class="section-padding">
                <div class="container card">
                    <div class="row white">
                        <div class="col-md-8 col-sm-12 section-head">
                            <div class="section-title">
                                <h4>1. Service Required &nbsp; &nbsp; &Tab;{{ $info->email }}</h4>

                            </div>
                        </div>
                        
                        <form action="insert.php" method="post" id="">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                        
                                <div class="col-md-4 top-10">
                                    <label>Applying for</label>
                                
                                    <div class="form-holder">
                                        
                                        <input class="form-control frmApp" name="1_application_for" id="1_application_for" value={{ $info->uniq_id }} readonly="">
                                        
                                    </div>

                                    <div class="clb"></div>
                                </div>
                                <div class="col-md-4 top-10">
                                    <label>Type of Application <span style="color:red">*</span> </label>
                                    <div class="form-holder">
                                        <input class="form-control " name="sel_type_appl" id="sel_type_appl" value={{ $info->email }} readonly="">
                                        
                                       
                                        <div id="divCheckPasswordMatch9"></div>
                                    </div>

                                    <div class="clb"></div>
                                </div>
                                <div class="col-md-4 top-10">
                                    <label>Type of Passport Booklet <span style="color:red">*</span> </label>
                                    <div class="form-holder">
                                        <input class="form-control " name="1_type_of_booklet" id="contact_1_type_of_booklet" value={{ $info->email }} readonly="">
                                        <div id="divCheckPasswordMatch10"></div>
                                    </div>
                                </div>

                                <div id="tatkal_note" style="display:none;">
                                    <div class="col-md-12 top-10">
                                        <p class="marq-blink text-center" style="font-size: 15px;"><b>Note: For Tatkal Scheme, you need to pay Additional Tatkal Fees of Rs. 2000/- at Passport Seva Kendra during your Appointment.</b></p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </section>  
    
                <!---------------------------------Section Second-------------------------------------------->
    
    <div class="container card">
        <div class="row white">
            <div class="col-md-8 col-sm-12 section-head">
                <div class="section-title">
                    <h4>2. Applicant Details</h4>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">

                    <div class="row">
                        <div class="col-md-4">
                            <label>Applicant's First Name <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control fname_2" id="contact_2_fname" maxlength="50" value={{ $info->email }} readonly="">
                                <span class="error" id="mylocation1"></span>
                                <span id="sp" style="color:red"></span>
                                <!--div id="sp" style="color:red"></div-->
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" id="txt_middlename" maxlength="50" value={{ $info->email }} readonly="">
                                <span id="error_middle_name" style="color:red"></span>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <label>Surname</label>
                            <div class="form-holder">
                                <input type="text" class="form-control" id="txt_lastname" maxlength="50" value={{ $info->email }} readonly="">
                                <span id="error_last_name" style="color:red"></span>
                            </div>
                        </div>
                    
                    </div>

                

                <div class="row">

                    <div class="col-md-4">
                        <label>Aadhaar No</label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_aadharno" maxlength="12" value={{ $info->email }} readonly="">
                            <span id="error_aadhar" style="color:red"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Gender <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_aadharno" maxlength="12" value={{ $info->email }} readonly="">
                            <div id="error_gender" style="color:red"></div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <label>Marital Status <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_aadharno" maxlength="12" value={{ $info->email }} readonly="">
                            <div id="marital" style="color:red"></div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Date of Birth <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_aadharno" maxlength="12" value={{ $info->email }} readonly="">
                            <div id="dob2" style="color:red"></div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <label>Is your Place of Birth out of India? <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_aadharno" maxlength="12" value={{ $info->email }} readonly="">
                            <span class="error" id="mylocation63"></span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label>Village or Town or City <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="contact_2_village" value={{ $info->email }} readonly="" >
                            <span class="error" id="mylocation5">This field is required</span>
                            <div id="village" style="color:red"></div>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Country <span style="color:red">*</span></label>
                        <div class="form-holder" id="othe_con" >
                            <input class="form-control" id="country" value={{ $info->country }} readonly="">
                        </div>

                    </div>

                    <div id="place_birth" style="display: block;">

                        <div class="col-md-4 top-10">
                            <label>State <span style="color:red">*</span></label>
                            <div class="form-holder">
                               <input type="text" class="form-control fname_2" id="contact_2_fname" maxlength="50" value={{ $info->state }} readonly="">
                                <div id="state2" style="color:red"></div>

                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>District <span style="color:red">*</span> </label>
                            <div class="form-holder">
                            
                                <input type="text" class="form-control fname_2" id="contact_2_fname" maxlength="50"  value={{ $info->district }} eadonly="">
                                
                                <div id="district2" style="color:red"></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>Citizenship of India by</label>
                        <div class="form-holder">
                            <input type="text" class="form-control fname_2" id="contact_2_fname" maxlength="50"  value={{ $info->citizenship_of_india_by }} readonly="">
                            <span id="error_citizenship" style="color:red"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>PAN (If available)</label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_pan" maxlength="10" style="text-transform:uppercase;"  value={{ $info->pan }} readonly="">
                            <span id="error_pan" style="color:red"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Voter ID (If available)</label>
                        <div class="form-holder">
                            <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->voter_id }} readonly="">
                            <span id="error_voter" style="color:red"></span>
                        </div>
                    </div>
                    

                </div>

                <div class="row">

                    <div class="col-md-4 top-10">
                        <label>Educational Qualification <span style="color:red">*</span></label>
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->educational }} readonly="">
                    </div>

                    <div class="col-md-4 top-10">
                        <label>Employment Type <span style="color:red">*</span> </label>
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->employment }} readonly="">

                    </div>
                    <div id="div_emptype" >
                        <div class="col-md-4 top-10">
                            <label>Organisation Name <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->organisation }} readonly="">
                                <span id="error_org" style="color:red"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>Is your parent (If Minor)/ spouse, a govt servant? <span style="color:red">*</span> </label>
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->is_your_parent }} readonly="">
                        </div>

                    
                    <div class="col-md-4">
                        <label>Are you eligible for Non-ECR category? <span style="color:red">*</span><sup data-toggle="modal" data-target="#myModal"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup>
                        </label>
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->non_ecr_category }} readonly="">

                    </div>
                    <div class="col-md-4">
                        <label>Visible Distinguishing Mark (If any?) <sup data-toggle="modal" data-target="#myModal2"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup></label>
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->body_mark }} readonly="">
                    </div>

                </div>
                
                
                <div class="row">
                    <div class="col-md-4">
                        <label>Are you known by any other names?</label>
                        <div class="form-holder">
                        <input type="text" class="form-control" id="txt_voter_id" maxlength="10"  value={{ $info->other_name }} readonly="">
                    
                        </div>
                    </div>
                </div>

                <div id="div_alises">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Alias Name1, alises First name <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper" id="txt_aliases_fname"  value={{ $info->alias_f }} readonly="">
                                <span id="error_alf" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label> Middle name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper"  value={{ $info->alias_m }} readonly="">
                                <span id="error_alm" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Surname</label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper"  value={{ $info->alias_l }} readonly="">
                                <span id="error_all" style="color:red"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clb"></div>
                    <div class="col-md-12 topf label-input"></div>

                    <div class="clb"></div>
                    <div class="col-md-12 topf label-input"></div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>Have you ever changed your name ? </label>
                        <div class="form-holder">
                            <input type="text" class="form-control text-upper"  value={{ $info->change_name }} readonly="">
                        </div>
                    </div>

                </div>

                <div id="div_chngname">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Previous Name1,alises First name <span style="color:red">*</span> </label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper"  value={{ $info->applying }} readonly="">
                                <span id="error_alpf" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Middle name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper"  value={{ $info->applying }} readonly="">
                                <span id="error_alpm" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Surname</label>
                            <div class="form-holder">
                                <input type="text" class="form-control text-upper"  value={{ $info->applying }} readonly="">
                                <span id="error_alpl" style="color:red"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>  
                        


    <section class="section-padding" id="contact">
        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>3. Family Details (Father/Mother/Legal Guardian details; at least one is mandatory.)</h4>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">

                        <div class="col-md-4">
                            <label>Father's First Name <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->father_f_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->father_m_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Surname </label>
                            <div class="form-holder">
                                <input type="text" class="form-control" value={{ $info->father_l_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-12 topf label-input"></div>

                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <label>Mother's First Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->mother_f_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->mother_m_name }}  readonly="">
                                <span id="error_myform_3_mother_m_name" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Surname </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->mother_l_name }}  readonly="">
                                <span id="error_myform_3_mother_l_name" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-12 topf label-input">

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-4 top-10">
                            <label>Legal Guardian's First Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->gard_f_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->gard_m_name }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Surname </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->gard_l_name }}  readonly="">
                            </div>
                        </div>

                    </div>

                    <div class="row" id="flio_lbl_marital">
                        <div class="col-md-4 top-10">
                            <label>Spouse First Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->spz_f_name }}  readonly="">
                                <span id="error_myform_3_spz_f_name" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Middle Name </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->spz_m_name }}  readonly="">
                                <span id="error_myform_3_spz_m_name" style="color:red"></span>
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Surname </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->spz_l_name }}  readonly="">
                                <span id="error_myform_3_spz_l_name" style="color:red"></span>
                            </div>
                        </div>
                    </div>

                    <div id="error_fathermother" style="color:red; text-align:center; padding-top: 20px;"></div>

                </div>

            </div>
            
        </div>
        
    </section>



    <!--Section 4-->

    <div class="container card">
        <div class="row white">
            <div class="col-md-8 col-sm-12 section-head">
                <div class="section-title">
                    <h4>4. Present Residential Address Details (where applicant presently resides)</h4>
                    <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">

                    <div class="col-md-4">
                        <label>Is your present address out of India? </label>
                        <input type="text" class="form-control"  value={{ $info->present_address_type }}  readonly="">
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>House No. and Street Name <span style="color:red">*</span> </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->house_no }}  readonly="">
                            <div id="houseno" style="color:red"></div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <label>Village or Town or City <span style="color:red">*</span> </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->town }}  readonly="">
                            <div id="town" style="color:red"></div>
                        </div>

                    </div>
                    <div>
                        <div class="col-md-4">
                            <label>Country <span style="color:red">*</span> </label>
                            <input type="text" class="form-control"  value={{ $info->country_4 }}  readonly="">

                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4 top-10 ">
                        <label>State/ Province <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->state_4 }}  readonly="">
                        </div>
                    </div>
                    <div class="col-md-4 top-10 ">
                        <label>District <span style="color:red">*</span> </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->district_4 }}  readonly="">
                        </div>
                    </div>

                    <div class="col-md-4 top-10">
                        <label>
                            <div class="label-input">Pin Code / Zip Code<span style="color:red">*</span></div>
                        </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->pincode }}  readonly="">
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <label>Police Station <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->police_station }}  readonly="">
                        </div>

                        <p class="marq-blink text-center" style="font-size: 12px;"><b>(Police Station may vary than you mentioned.)</b></p>
                    </div>
                    <div class="col-md-4">
                        <label>Mobile No <span style="color:red">*</span></label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->mobile }}  readonly="">
                        </div>

                    </div>
                    <div class="col-md-4">
                        <label>E-mail ID <span style="color:red">*</span> </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->email }}  readonly="">
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4 top-10">
                        <label>Do you have a Permanent Address? </label>
                        <div class="form-holder">
                            <input type="text" class="form-control"  value={{ $info->permanent_address }}  readonly="">
                            <span class="error" id="mylocation77"></span>
                        </div>
                    </div>

                </div>

                <div id="div_perm_yn_adr" >
                    <div class="clb"></div>
                    <div class="row">
                        <div class="col-md-4 top-10">
                            <label>Is permanent address same as present address? </label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->permanent_address_type }}  readonly="">
                            </div>
                        </div>
                    </div>

                    <div class="clb"></div>
                    <div id="div_permanent_adr">
                        <div class="row">
                            <div class="col-md-4 top-10">
                                <label>House No. and Street Name <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control"  value={{ $info->p_house_no }}  readonly="">
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Village or Town or City <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control"  value={{ $info->p_town }}  readonly="">
                                </div>
                            </div>
                            <div class="col-md-4 top-10">
                                <label>Country <span class="marq-blink text-center" style="font-size: 12px;"><b>(If not in India.)</b></span></label>
                                <input type="text" class="form-control" value={{ $info->p_country }}   readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="clb"></div>
                        <div class="row">
                            <div class="col-md-4 top-10 per_mds" style="display: block;">
                                <label>State/ Province <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control"  value={{ $info->p_state }}  readonly="">
                                </div>
                            </div>
                            <div class="col-md-4 top-10 per_mds" style="display: block;">
                                <label>District <span style="color:red">*</span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control"  value={{ $info->p_district }}  readonly="">
                                </div>
                            </div>

                            <div class="col-md-4 top-10">
                                <label>Pin <span style="color:red">*</span> </label>
                                <div class="form-holder">
                                    <input type="text" class="form-control" value={{ $info->p_pincode }}   readonly="">
                                </div>
                            </div>
                        </div>

                        <div class="clb"></div>
                        <div class="row">
                            <div class="col-md-4 top-10">
                                <label>Police Station <span style="color:red">* </span></label>
                                <div class="form-holder">
                                    <input type="text" class="form-control"  value={{ $info->p_police_station }}  readonly="">
                                </div>

                                <span class="marq-blink text-center" style="font-size: 12px;"><b>(Police Station may vary than you mentioned.)</b></span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->


    <section class="section-padding" id="contact">
        <div class="container card">
            <div class="row white">
                <div class="col-md-8 col-sm-12 section-head">
                    <div class="section-title">
                        <h4>5. Emergency Contact Details</h4>
                        <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-holder">
                                <label for="male">Name And Address<span style="color:red">*</span></label>
                                <input type="text" class="form-control"  value={{ $info->name_and_address }}  readonly="">
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-holder">
                                <label for="male">Mobile No<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value={{ $info->mobile_no }}   readonly="">
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-holder">
                                <label for="male">E-mail ID</label>
                               <input type="text" class="form-control"  value={{ $info->email_id }}  readonly="">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <div class="container card">
        <div class="row white">
            <div class="col-md-8 col-sm-12 section-head">
                <div class="section-title">
                    <h4>6. Previous Passport/ Application Details</h4>
                    <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <label >Have you ever held/hold any Identity Certificate? <span style="color:red">*</span> <sup data-toggle="modal" data-target="#myModal3"><i style="font-size: 20px; color: #ff6613;" class="fa fa-question-circle"></i></sup></label>
                        <div class="form-holder">
                            <input style="color:red" type="text" class="form-control"  value={{ $info->hold_any_Identity }}  readonly="">
                        </div>
                    </div>

                </div>

                <div class="clb"></div>

                <div id="div_changeIC">
                    <div class="row">
                        <div class="col-md-4 top-10">
                            <label>Identity Certificate/Passport Number <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->passport_number }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Date of Issue(DD-MM-YYYY) <span style="color:red">*</span></label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->date_of_issue }}  readonly="">
                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Date of Expiry(DD-MM-YYYY) <span style="color:red">*</span> </label>
                            <input type="text" class="form-control"  value={{ $info->date_of_expiry }}  readonly="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 top-10">
                            <label>Place of Issue <span style="color:red">*</span> </label>
                            <input type="text" class="form-control"  value={{ $info->place_of_issue }}  readonly="">
                        </div>
                        <div class="col-md-4 top-10">
                            <label>File Number</label>
                            <input type="text" class="form-control"  value={{ $info->file_number }}  readonly="">
                        </div>
                    </div>
                </div>


                <div id="div_chngissue" >
                    <div class="row">
                        <div class="col-md-4 top-10">
                            <label>File Number</label>
                            <div class="form-holder">
                                <input type="text" class="form-control"  value={{ $info->file_number_not_iss }}  readonly="">

                            </div>
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Month and year Applying <span style="color:red">*</span></label>
                            <input type="text" class="form-control"  value={{ $info->month_and_year_applying }}  readonly="">
                        </div>
                        <div class="col-md-4 top-10">
                            <label>Name of passport office where applied <span style="color:red">*</span></label>
                            <input type="text" class="form-control"  value="{{ $info->pass_office_where_applied }}" readonly="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>    

<br>

    <div class="container card">
        <div class="row white">
            <div class="col-md-8 col-sm-12 section-head">
                <div class="section-title">
                    <h4>8. Documents Submitted as Proof</h4>
                    <!--hr class="botm-line">
            <p class="sec-para black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p-->
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-holder">
                                    <label for="male">Address Proof</label>
                                    <input type="text" class="form-control"  value={{ $info->address_proof_7 }}  readonly="">
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-holder">
                                    <label for="male">Date of Birth Proof</label>
                                    <input type="text" class="form-control"  value={{ $info->d_o_b_proof_7 }}  readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
<br>

    </div>
    </div>




    <!--contact ends-->
    <style>
        a {
            padding-left: 10px;
        }
    </style>

    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/custom.js"></script>
    <script src="contactform/contactform.js"></script>

    <script src="js/form_validate.js"></script>

</body>


</html>