@extends('layouts.app')


<div class="header">
    <div class="container">
        <div class="top-head pt20">
            <div class="col-md-4 col-sm-4 hidden-xs">
                <a href="#"><img class="logo-img" src="image/Passportlogo.png"></a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="center-head">
                    <h3 class="chead1">ऑनलाइन पासपोर्ट पंजीकरण</h3>
                    <h3 class="chead2"> Passport Online Application Support</h3>
                    <h3 class="chead3">(Private Consultancy Service)</h3>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="seatch_bharat">
                    <img class="logo-img" src="image/Artboard1.png">
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-inverse mt10">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="image/logowhite.svg" class="mobi-logo visible-xs">
        </div>
        <div class="container">
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="https://thepassportindia.org/">New Passport</a></li>
                    <li><a href="https://thepassportindia.org/">Reissue Passport</a></li>
                    <li><a href="https://thepassportindia.org/process">Procedure</a></li>
                    <li><a href="https://thepassportindia.org/documents">Required Documents</a></li>
                    <li><a href="https://thepassportindia.org/fees">Fees</a></li>
                    <li><a href="https://thepassportindia.org/faq">FAQ's</a></li>                    
                    <li><a href="https://thepassportindia.org/contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>


