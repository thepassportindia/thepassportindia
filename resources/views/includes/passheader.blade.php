@extends('layouts.app')

    <header class="main-header" id="header">
        <div class="bg-color">
            <!--nav-->

            <div class="row navbar-fixed-top top-logo">
                <div class="col-lg-3 col-md-3 col-sm-12 img-1" style="margin-top:-10px;">
                    <a href="index.html"><img src="img/logo.png" alt="" title="" style="margin-top:-20px;">
                        <p style="margin-top:-20px; padding-left:75px;">Private Consultancy</p>
                    </a>
                </div>

                <div class="col-lg-6 col-md-6 img-2">
                    <h3 style="color: #000; font-weight:700; font-family: 'Mukta', sans-serif; font-family: 'Martel', serif;">पासपोर्ट ऑनलाइन आवेदन सहायता </h3>
                    <h3 style="color: #000; font-weight:700; font-family: 'Mukta', sans-serif; font-family: 'Martel', serif;">Passport Online Application Support </h3>
                    <h5 style="color: #000; font-weight:700; font-family: 'Mukta', sans-serif; font-family: 'Martel', serif;"> (An ISO 9001:2015 Certified Organization)</h5></5>
                </div>

                <div class="col-lg-3 col-md-3 img-3">
                    <img src="swaksh-bharat1.png" alt="" title="" height="130" width="250">
                </div>
            </div>

            <nav class="nav navbar-default navbar-fixed-top">

                <div class="containe">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <div class="col-lg-6 col-md-4 img-21">
                                <h3 style="text-align:left; color: #ffffff; font-weight:700; font-family: 'Mukta', sans-serif; font-family: 'Martel', serif;">पासपोर्ट ऑनलाइन आवेदन सहायता </h3>
                                <h3 style="text-align:left; color: #ffffff; font-weight:700; font-family: 'Mukta', sans-serif; font-family: 'Martel', serif;">Passport Online Application Support </h3>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse navbar-light" id="mynavbar">
                            <ul class="nav navbar-nav">

                                <li class="menu-active"><a href="index.html">NEW APPLICATION</a></li>
                                <li><a href="process.html">PROCESS</a></li>
                                <li><a href="documents.html">REQUIRED DOCUMENTS</a></li>
                                <li><a href="annexure.html">ANNEXURE'S</a></li>
                                <li><a href="faq.html">FAQ'S</a></li>
                                <li><a href="fee.html">FEES</a></li>
                                <li><a href="contact.html">CONTACT</a></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </nav>

        </div>
    </header>
