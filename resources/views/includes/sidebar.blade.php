<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Hi, Chirag</h3>
    </div>
    <ul class="list-unstyled components">
        <li class="active">
            <a style="color: #ffffff;" href="{{ url('/output')}}">Home</a>
        </li>
        <li class="active">
            <a style="color: #ffffff;" href="{{ url('/output/onlymale')}}" >Only Male</a>
        </li>
        <li>
            <a style="color: #ffffff;" href="{{ url('output/onlyfemale')}}" >Only Female</a>
        </li>

        <li>
            <a style="color: #ffffff;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout
            </a>
        </li>
    </ul>

</nav>