@extends('layouts.app')
<!-- Sidebar Holder -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Hello, Admin</h3>
    </div>
    <ul class="list-unstyled components">
        <li class="active">
            <a style="color: #ffffff;" href="{{ url('/passdashboard')}}">Home</a>
        </li>
        <li>
            <a style="color: #ffffff;" href="{{ url('passdashboard/newpassport')}}" >New Passport</a>
        </li>
        <li>
            <a style="color: #ffffff;" href="{{ url('passdashboard/reissue')}}" >Re-issue Passport</a>
        </li>
        <li>
            <a style="color: #ffffff;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout
            </a>
        </li>
    </ul>
</nav>

    