    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="passport application form, passport seva Kendra, passport
application online, Passport Correction, Passport Renewal, Passport
reissue, tatkal passport, new passport,">

    <title>Passport Online Apply In INDIA</title>

    <link href="passport-glify.jpg" rel="icon">

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/app.') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/main.') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style1.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/error.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom_css.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('js/newapply.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/newapply_reissue.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/custom.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/bootstrap.min.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/jquery.min.js') }}" rel="stylesheet" type="text/js" />

    <link href="{{ asset('js/jquery.easing.min.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/form_validate.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/validate.js') }}" rel="stylesheet" type="text/js" />
    <link href="{{ asset('js/wow.js') }}" rel="stylesheet" type="text/js" />


    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145237464-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145237464-1');
</script>
