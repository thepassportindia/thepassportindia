   <footer class="" id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-lg-4 footer-copyright">
          © 2019 Online Passport. All Right Reserved
          <!--div class="credits">
            <
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Bethany
            >
            Designed by : <a href="https://acensoft.com" target="_blank">AcenSoft IT Solutions</a>
          </div-->
        </div>
        
        <div class="col-sm-12 col-lg-8">
          <div class="pull-right">
          <div class="foot-nav"> 
		  <a href="https://thepassportindia.org/about"> About Us</a>
          <a href="https://thepassportindia.org/terms">Terms & Conditions</a>
          <a href="https://thepassportindia.org/policies">Policies</a>
          <a href="https://thepassportindia.org/desclaimer">Disclaimer</a>
          <a href="https://thepassportindia.org/refund">Refund Policy</a>
		  </div>     
          </div>
        </div>
        
      </div>
    </div>
  </footer>