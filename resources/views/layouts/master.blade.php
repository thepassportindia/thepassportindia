<html>
<head>
    @include('includes.head')
</head>
<body>
	<div class="wrapper" style="padding-top:0;">
		@include('includes.header')
		
		<div id="main" class="col-xs-12">
			<div class="col-xs-12">
    <div class="col-sm-12 col-xs-12">
        <h1>Search</h1>
    </div>
</div> 
<div class="col-sm-12 col-xs-12">
        <form class="input-form" id="filterForm" action="{{route('search')}}" method="POST" role="search">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input id="chargeback" type="text" name="name" value="" class="form-control" aria-label="Text input with dropdown button">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">
                                Search
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>  
	        @yield('content')
	    </div>
    </div>   
</body>
</html>