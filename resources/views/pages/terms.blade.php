<!DOCTYPE html>
<html lang="en">

@include('includes.head')
   
<script type="text/javascript">
$(document).ready(function(){


    $('#contact_2_state').on('change',function(){		
        var stateID = $(this).val();
		
		//alert (qualificationID);
		
        if(stateID){
            $.ajax({
                type:'POST',
                url:'locationData.php',		
                
                data:'state_id='+stateID,
                success:function(html){
                    $('#contact_2_district').html(html);
                    
                }
            }); 
        }else{
            $('#contact_2_district').html('<option value="">Select state first</option>');
           
        }
    });
 
});

//End script

</script>
  <style>
  h1{
       text-align:center; 
       color: #000;"
      
  }
  h5 {
    margin-top: 30px;
    text-align: center;
  }
  
  marquee{
     background-color: #5DA6D3;
     font-size: 16px;
     padding-top:5px;
     padding-bottom:5px;
     color: #000; 
     font-weight: 700;
     letter-spacing: 3px;
  }
  .img-11{
     display:none;
 }
 .img-21{
     display:none;
 }
 .img-2>h3{
	 text-align:center;
	 font-size:30px;
	 margin-bottom: -20px;
 }
 
   .section-head{
       background-color: #5DA6D3;
       width:100%; 
       #border-radius: 10px;
       margin-bottom: 15px;
   }
   
  .card{ 
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    #border: 2px solid #686868;
    #border-radius: 15px;
	
	    -webkit-box-shadow: 10px 10px 15px 2px #686868;
	}
	h2{
	color: #1dc8cd;
	}
	h4{
	    color:white;
	}
	.enq{
	    padding-left: 80px;
	    color: #ffffff;
	    font-size:20px;
	}
	.enq-bg{
	#background: #ff8923;
	background: #2e5fa7;
    padding: 10px 10px 10px 10px;
}
.marq-blink {
    #animation:blinkingText 1s infinite;
	}
	
@keyframes blinkingText{
    0%{     color: #000;    }
    #49%{    color: transparent; }
    #50%{    color: transparent; }
    #99%{    color:transparent;  }
    100%{   color: #f28727;    }
}
p{
    color: black;
}
.error{
color: red;
}

	.navbar-default{
	margin-top:140px;
	
	}
	
	.top-logo{
	background-color:#ffffff;
	#margin-left:20px;
	padding-left:30px;
	margin-right:-10px;
	background:cover;
	}
	
	
  .faq-head{
  background: gray;
  padding-top:10px;
  padding-bottom: 10px;
  }
  .btn-warning{
      background: #3e70cb;
  }

 @media only screen and (max-width: 600px) {
	 
	
   .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 10px;
	padding-top: 5px;
	padding-bottom: 5px;
	}
   .main-header{   
     min-height: 110px;
	}
 	
  .img-1{
     display:none;
     height:10px;
     width:10px;
  }
  .img-2{
	  display:none;
      #font:10px;
      #text-align:right;
      #padding-left:10px;
     
   }
   .img-21{
      display:block;
	}
	
 .img-21>h3{
       font-size:16px;
       text-align:left;
       
   }
   
   .img-2 h3{
       font-size: 20px;
       text-align:right;
       margin-top:10px;
    }
   
  .img-3{
     
      display:none;
  }
  .img-11{
     display: none;
     #height:15px;
     #width:15px;
     #margin-left:5px;
  }
  .top-nav-collapse {

	  #padding: 25px 0;
	  }
	.navbar-default {
		margin-top: 0px;
	}
	.form-control {
		margin-top:10px;
	}
	h1{
		font-size:25px;
	}

}


</style>


<style>
    .error{
			display: none;
		
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		input.invalid, textarea.invalid{
		    
		    border:2px solid red;
		}
		
			input.valid, textarea.valid{
		    
		    border:2px solid green;
		}
</style>

</head>
<body>
  <!--header-->
  
  @include('includes.header1')
  <!--/ header-->

  <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>
	<!--div class="row enq-bg">
        	
            <div class="col-md-2 enq-title top-10">
            	<label class="enq">Enquiry</label>
            </div>
            
            <div class="col-md-10 unic">
                
                <form  role="form" method="post" action="enquiry_db.php">
                    
            	<div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Name" name="name" id="txt_top_name" maxlength="100" autocomplete="off" required pattern= "/^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*/" title= "Minimum 1 digit,number not allow"></div>
                
                <div class="col-md-3 top-10"><input type="email" class="form-control center-block" placeholder="Email" name="email" id="txt_top_email" maxlength="100" required></div>
                
                <div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Phone" name="phone" id="txt_top_phone" maxlength="10" pattern="[6789][0-9]{9}" title="Phone number with 6-7-8-9 and remaing 9 digit with 0-9" required></div>
                <div class="col-md-3 top-10"><button type="submit" name="submit" class="btn btn-warning form-control">Submit</button></div>
                </form>
            </div>
    </div-->
    <!---->
  <section class="section-padding wow fadeInUp delay-05s" id="contact">
    <div class="container card">
      <div class="row white">
        
		
        <div class="col-md-12 col-sm-12">
						<div class="row">
						    
						 <h1 style="text-align:center; color: #ff6613;">Terms & Conditions</h1>
                            <hr size="4px">
   
					
		<p class="pse" style="padding-left:40px; padding-right:20px;">Welcome to passportonlineindia.org by visiting this website and accessing the information,
		resources, service and tools We provide, you understand and agree to accept and adhere to the following 
		terms and conditions as stated in this Policy (hereafter referred to as 'user agreement'), along with the
		terms and conditions as stated in our privacy Policy (please refer to the privacy policy section below for more information).</p>
<p class="pse" style="padding-left:40px; padding-right:20px;">We reserve the right to change this user agreement from time to time without notice.
You acknowledge and agree that it is your responsibility to review this user agreement periodically to familiarize yourself with any modifications.
Your continued use of this site after such modifications will constitute acknowledgment and Agreement of the modified terms and conditions.</p>

<h4 style="color: #ff6613; text-align:left; padding-left:20px;">1. Responsible Use & Conduct</h4>
    <p class="pse" style="padding-left:40px; padding-right:20px;">In order to access our resources, you may be required to provide certain 
    information about yourself (such as Identification, email, phone number, contact details, etc.) as part of the registration process, 
    or as part of your Ability to use the resources. You agree that any information you provide will always be
    accurate, correct, and up to date.</p>
    
<p class="pse" style="padding-left:40px; padding-right:20px;">•	Accessing (or attempting to access) any of our resources by any means other than through the means
we provide, is strictly prohibited. You specifically agree not to access (or attempt to access) any of our 
resources through any Automated, unethical or unconventional means.</p>

<p class="pse" style="padding-left:40px; padding-right:20px;">•	Engaging in any activity that disrupts or interferes with our resources, including the servers 
and/or networks to which our resources are located or connected, is strictly prohibited.</p>

<p class="pse" style="padding-left:40px; padding-right:20px;">•	Attempting to copy, duplicate or reproduce our resources is strictly prohibited.</p>
<p class="pse" style="padding-left:40px; padding-right:20px;">•	You are solely responsible any consequences, losses, or damages that we may directly or 
indirectly incur or suffer due to any unauthorized activities conducted by you, as explained above,
and may incur criminal or civil Liability.</p>

                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">2. Privacy</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">Your privacy is very important to us, which is why we've created a separate privacy
                policy in order to explain in detail How we collect, manage, process, secure, and store 
                your private information. Our privacy policy is included under the Scope of this user agreement.</p>
                
                 <h4 style="color: #ff6613; text-align:left; padding-left:20px;">3. Limitation of Liability</h4>
                 <p class="pse" style="padding-left:40px; padding-right:20px;">By using this website, you expressly understand and agree that any claim against us shall be 
                 limited to the amount you paid, if any, for use of services. www.passportonlineindia.org will not 
                 be liable for any direct, indirect, incidental, consequential or exemplary loss or damages which may
                 be incurred by you as a result of using our resources, or as a Result of any changes, data loss or corruption,
                 cancellation, loss of access, or downtime to the full extent that Applicable limitation of liability laws apply.</p>
                 
                 <h4 style="color: #ff6613; text-align:left; padding-left:20px;">4. Termination of Use</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">You agree that we may, at our sole discretion, suspend or terminate your access to all or part of our website and
                Resources with or without notice and for any reason, including, without limitation, breach of this user agreement. 
                Any suspected illegal, fraudulent or abusive activity may be grounds for terminating your relationship and may be
                Referred to appropriate law enforcement authorities. Upon suspension or termination, your right to use the Resources
                we provide will immediately cease, and we reserve the right to remove or delete any information that you May have on 
                file with us, including any account or login information.</p>

                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">5. Governing Law</h4>
 
                <p class="pse" style="padding-left:40px; padding-right:20px;">By accessing our website, you agree that the statutes and laws of India will apply to all matters relating to the use of this website.</p>
                
                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">6. Cancellation and Refund</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">Cancellation of application is not possible once the payment has been made. No refunds will be given except
                in the Event of cancellation or non-performance of service by www.thepassportindia.org.
                </p>
                
                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">7. Guarantee</h4>

                <p class="pse" style="padding-left:40px; padding-right:20px;">Unless otherwise expressed, www.thepassportindia.org expressly disclaims all warranties and conditions of 
                any kind, whether express or implied, including, but not limited to the implied warranties and conditions of
                merchantability, fitness for a particular purpose and non-infringement.</p>

				</div>
			</div>
		</div>
	</div>
    </section>   
		 


  
@include('includes.footer')
  <!---->
  <!--contact ends-->
  <style>
  a{
  padding-left: 10px;
  }
  
  </style>
  
  
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

<script src="js/form_validate.js"></script>




</body>

<!-- Mirrored from passportonlineindia.org/terms.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:46 GMT -->
</html>
