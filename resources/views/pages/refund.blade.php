<!DOCTYPE html>
<html lang="en">

@include('includes.head')
   
<script type="text/javascript">
$(document).ready(function(){


    $('#contact_2_state').on('change',function(){		
        var stateID = $(this).val();
		
		//alert (qualificationID);
		
        if(stateID){
            $.ajax({
                type:'POST',
                url:'locationData.php',		
                
                data:'state_id='+stateID,
                success:function(html){
                    $('#contact_2_district').html(html);
                    
                }
            }); 
        }else{
            $('#contact_2_district').html('<option value="">Select state first</option>');
           
        }
    });
 
});

//End script

</script>
  <style>
  h1{
       text-align:center; 
       color: #000;"
      
  }
  h5 {
    margin-top: 30px;
    text-align: center;
  }
  
  marquee{
     background-color: #5DA6D3;
     font-size: 16px;
     padding-top:5px;
     padding-bottom:5px;
     color: #000; 
     font-weight: 700;
     letter-spacing: 3px;
  }
  .img-11{
     display:none;
 }
 .img-21{
     display:none;
 }
 .img-2>h3{
	 text-align:center;
	 font-size:30px;
	 margin-bottom: -20px;
 }
 
   .section-head{
       background-color: #5DA6D3;
       width:100%; 
       #border-radius: 10px;
       margin-bottom: 15px;
   }
   
  .card{ 
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    #border: 2px solid #686868;
    #border-radius: 15px;
	
	    -webkit-box-shadow: 10px 10px 15px 2px #686868;
	}
	h2{
	color: #1dc8cd;
	}
	h4{
	    color:white;
	}
	.enq{
	    padding-left: 80px;
	    color: #ffffff;
	    font-size:20px;
	}
	.enq-bg{
	#background: #ff8923;
	background: #2e5fa7;
    padding: 10px 10px 10px 10px;
}
.marq-blink {
    #animation:blinkingText 1s infinite;
	}
	
@keyframes blinkingText{
    0%{     color: #000;    }
    #49%{    color: transparent; }
    #50%{    color: transparent; }
    #99%{    color:transparent;  }
    100%{   color: #f28727;    }
}
p{
    color: black;
}
.error{
color: red;
}

	.navbar-default{
	margin-top:140px;
	
	}
	
	.top-logo{
	background-color:#ffffff;
	#margin-left:20px;
	padding-left:30px;
	margin-right:-10px;
	background:cover;
	}
	
	
  .faq-head{
  background: gray;
  padding-top:10px;
  padding-bottom: 10px;
  }
  .btn-warning{
      background: #3e70cb;
  }

 @media only screen and (max-width: 600px) {
	 
	
   .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 10px;
	padding-top: 5px;
	padding-bottom: 5px;
	}
   .main-header{   
     min-height: 110px;
	}
 	
  .img-1{
     display:none;
     height:10px;
     width:10px;
  }
  .img-2{
	  display:none;
      #font:10px;
      #text-align:right;
      #padding-left:10px;
     
   }
   .img-21{
      display:block;
	}
	
 .img-21>h3{
       font-size:16px;
       text-align:left;
       
   }
   
   .img-2 h3{
       font-size: 20px;
       text-align:right;
       margin-top:10px;
    }
   
  .img-3{
     
      display:none;
  }
  .img-11{
     display: none;
     #height:15px;
     #width:15px;
     #margin-left:5px;
  }
  .top-nav-collapse {

	  #padding: 25px 0;
	  }
	.navbar-default {
		margin-top: 0px;
	}
	.form-control {
		margin-top:10px;
	}
	h1{
		font-size:25px;
	}

}


</style>


<style>
    .error{
			display: none;
		
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		input.invalid, textarea.invalid{
		    
		    border:2px solid red;
		}
		
			input.valid, textarea.valid{
		    
		    border:2px solid green;
		}
</style>

</head>

<body>
  <!--header-->
    @include('includes.header1')
  <!--/ header-->


  <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>
	<!--div class="row enq-bg">
        	
            <div class="col-md-2 enq-title top-10">
            	<label class="enq">Enquiry</label>
            </div>
            
            <div class="col-md-10 unic">
                
                <form  role="form" method="post" action="enquiry_db.php">
                    
            	<div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Name" name="name" id="txt_top_name" maxlength="100" autocomplete="off" required pattern= "/^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*/" title= "Minimum 1 digit,number not allow"></div>
                
                <div class="col-md-3 top-10"><input type="email" class="form-control center-block" placeholder="Email" name="email" id="txt_top_email" maxlength="100" required></div>
                
                <div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Phone" name="phone" id="txt_top_phone" maxlength="10" pattern="[6789][0-9]{9}" title="Phone number with 6-7-8-9 and remaing 9 digit with 0-9" required></div>
                <div class="col-md-3 top-10"><button type="submit" name="submit" class="btn btn-warning form-control">Submit</button></div>
                </form>
            </div>
    </div-->
    <!---->
  <section class="section-padding wow fadeInUp delay-05s" id="contact">
    <div class="container card">
      <div class="row white">
        
		
        <div class="col-md-12 col-sm-12">
						<div class="row">
						    
						 <h1 style="text-align:center; color: #ff6613;">Refund Policy</h1>
                            <hr size="4px">
  
					
					<h4 style="color: #ff6613; text-align:left; padding-left:20px;">1.	Refund of Payment Received</h4>
		<p class="pse" style="padding-left:40px; padding-right:20px;">we thepassportindia.org will be held liable for 
		refund of money received, only in case of our inability to render services, also it is hereby agreed by you that refund will be processed only after deduction of 20% from the amount received, further also the deducted amount will be treated as our services charges of banking and third party payment gateway, The refund process will be initiated once the confirmation of the services are provided. 
		In case of refund request accepted, amount will be refunded in the same mode you have paid. </p>
		


<h4 style="color: #ff6613; text-align:left; padding-left:20px;">2.  Refund Request</h4>
    <p class="pse" style="padding-left:40px; padding-right:20px;">Refund request can be sent at thepassportindia@gmail.com Refund request can be made within 10 days of online 
    application made.</p>

                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">3.	Refund Time Frame</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">We will process your refund request within 10 to 15 business days of receiving all the information required for processing 
                refund like reason for refund, bank details for processing request, etc.</p>
                
                 <h4 style="color: #ff6613; text-align:left; padding-left:20px;">4.	Cancellation of Application</h4>
                 <p class="pse" style="padding-left:40px; padding-right:20px;">You cannot cancel the application once it is processed.</p>
                 
                 <h4 style="color: #ff6613; text-align:left; padding-left:20px;">5.	Certification About Application</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">If you have any query about application process, you can write us mail @ thepassportindia@gmail.com 
                In case we need any additional clarification about your application our team will reach you by email or call.</p>
                
                <h4 style="color: #ff6613; text-align:left; padding-left:20px;">6.	Force Majeure</h4>
 
                <p class="pse" style="padding-left:40px; padding-right:20px;">thepassportindia.org shall not be considered in breach of its Satisfaction Guarantee 
                policy or default under any terms of service, and shall not be liable to the Client for any cessation, interruption, or delay in the performance of its 
                obligations by reason of earthquake, flood, fire, storm, lightning, drought, landslide, hurricane, cyclone, typhoon, tornado, natural disaster, act of 
                God or the public enemy, epidemic, famine or plague, action of a court or public authority, change in law, explosion, war, terrorism, armed conflict, 
                labour strike, lockout, boycott or similar event beyond our reasonable control, whether foreseen or unforeseen (each a "Force Majeure Event").</p>
                
                
                
		
	
	
				</div>
			</div>
		</div>
	</div>
    </section>   
		 


  
 @include('includes.footer')
  <!--contact ends-->
  <style>
  a{
  padding-left: 10px;
  }
  
  </style>
  
  
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

<script src="js/form_validate.js"></script>




</body>

<!-- Mirrored from passportonlineindia.org/refund.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:46 GMT -->
</html>
