<!DOCTYPE html>
<html lang="en">

@include('includes.head')

<style>
  h1{
       text-align:center; 
       color: #000;"
      
  }
  h5 {
    margin-top: 30px;
    text-align: center;
  }
  
  marquee{
     background-color: #5DA6D3;
     font-size: 16px;
     padding-top:5px;
     padding-bottom:5px;
     color: #000; 
     font-weight: 700;
     letter-spacing: 3px;
  }
  .img-11{
     display:none;
 }
 .img-21{
     display:none;
 }
 .img-2>h3{
	 text-align:center;
	 font-size:30px;
	 margin-bottom: -20px;
 }
 
   .section-head{
       background-color: #5DA6D3;
       width:100%; 
       #border-radius: 10px;
       margin-bottom: 15px;
   }
   
  .card{ 
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    #border: 2px solid #686868;
    #border-radius: 15px;
	
	    -webkit-box-shadow: 10px 10px 15px 2px #686868;
	}
	h2{
	color: #1dc8cd;
	}
	h4{
	    color:white;
	}
	.enq{
	    padding-left: 80px;
	    color: #ffffff;
	    font-size:20px;
	}
	.enq-bg{
	#background: #ff8923;
	background: #2e5fa7;
    padding: 10px 10px 10px 10px;
}
.marq-blink {
    #animation:blinkingText 1s infinite;
	}
	
@keyframes blinkingText{
    0%{     color: #000;    }
    #49%{    color: transparent; }
    #50%{    color: transparent; }
    #99%{    color:transparent;  }
    100%{   color: #f28727;    }
}
p{
    color: black;
}
.error{
color: red;
}

	.navbar-default{
	margin-top:140px;
	
	}
	
	.top-logo{
	background-color:#ffffff;
	#margin-left:20px;
	padding-left:30px;
	margin-right:-10px;
	background:cover;
	}
	
	
  .faq-head{
  background: gray;
  padding-top:10px;
  padding-bottom: 10px;
  }
  .btn-warning{
      background: #3e70cb;
  }

 @media only screen and (max-width: 600px) {
	 
	
   .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 10px;
	padding-top: 5px;
	padding-bottom: 5px;
	}
   .main-header{   
     min-height: 110px;
	}
 	
  .img-1{
     display:none;
     height:10px;
     width:10px;
  }
  .img-2{
	  display:none;
      #font:10px;
      #text-align:right;
      #padding-left:10px;
     
   }
   .img-21{
      display:block;
	}
	
 .img-21>h3{
       font-size:16px;
       text-align:left;
       
   }
   
   .img-2 h3{
       font-size: 20px;
       text-align:right;
       margin-top:10px;
    }
   
  .img-3{
     
      display:none;
  }
  .img-11{
     display: none;
     #height:15px;
     #width:15px;
     #margin-left:5px;
  }
  .top-nav-collapse {

	  #padding: 25px 0;
	  }
	.navbar-default {
		margin-top: 0px;
	}
	.form-control {
		margin-top:10px;
	}
	h1{
		font-size:25px;
	}

}


</style>


<style>
    .error{
			display: none;
		
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		input.invalid, textarea.invalid{
		    
		    border:2px solid red;
		}
		
			input.valid, textarea.valid{
		    
		    border:2px solid green;
		}
</style>

</head>
<body>

@include('includes.header1')
 
  <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>
  <section class="section-padding" id="contact">
    <div class="container card">
      <div class="row white">
        
		
        <div class="col-md-12 col-sm-12">
						<div class="row">
						 <h1>Required Documents</h1>
  <hr size="4px">
  
			<div class="table-responsive">          
  <table class="table">
    <thead style="font-size:20px; font-weight:700;">
      <tr>
       
        <th>Proof of Identity</th>
        <th>Proof of Address</th>
        <th>Proof of DOB</th>
        <th>Proof Education / Non-ECR</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        
      
        <td>
        	<li>Pan Card</li>
            <li>Aadhar Card</li>
            <li>Driving Licence</li>
            <li>Voter ID</li>
        </td>
        <td><li>Aadhaar Card</li>
                                    <li>Electricity Bill</li>
                                    <li>Telephone Bill</li>
                                    <li>Water Bill</li>
                                    <li>Spouse Passport</li>
                                    <li>Parents Passport</li>
                                    <li>Rent Agreement</li>
                                    <li>Bank Account Passbook</li>
                                    <li>Gas Connection Bill</li>
                                    <li>IT Assessment Order</li>
                                    <li>Employer Certificate</li></td>
        <td>	<li>Pan Card</li>
                                    <li>Aadhar Card</li>
                                    <li>Driving Licence</li>
                                    <li>Voter ID</li>
                                    <li>Birth Certificate</li>
                                    <li>Transfer/School Leaving Certificate</li>
                                    <li>Matriculation/10th/12th Certificate</li>
                                    <li>Service Record/Pay Pension Order</li>
                                    <li>Policy Bond</li>
                                    <li>Orphan Declaration</li></td>
        <td><li>Matriculation Certificate</li>
									<li>10th Certificate</li>
									<li>12th Certificate</li>
									<li>Higher Educational Pass Certificate</li></td>
      </tr>
      
    </tbody>
    
      
   
  </table>
  </div>

            
            <!--div class="clb"></div>
          	<div class="col-md-12 top-15 data-text">
            	List Of Proofs Required For <strong>Identity Proof, Address Proof, Date of Birth Proof, Education Proof / Non-ECR Proof</strong> : 
            </div>
            <div class="clb"></div>
            <div class="col-md-12 top-15 data-text">
            	<strong>1. Documents Type</strong>
            </div>
            <div class="clb"></div>
            <div class="col-md-12 top-15">
           		 <div class="panel with-nav-tabs panel-primary d-shadow bord-clr">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1primary" data-toggle="tab">Proof of Identity</a></li>
                        <li><a href="#tab2primary" data-toggle="tab">Proof of Address</a></li>
                        <li><a href="#tab3primary" data-toggle="tab">Proof of DOB</a></li>
                        <li><a href="#tab4primary" data-toggle="tab">Proof Education / Non-ECR </a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1primary">
                        	<div class="col-md-12 top-10">
                                <ul class="top-10">
                                	<li>Pan Card</li>
                                    <li>Aadhar Card</li>
                                    <li>Driving Licence</li>
                                    <li>Voter ID</li>
                                </ul>
                                <br>
                            </div>
                            <div class="clb"></div>
                        </div><!--end tab1primary >
                        <div class="tab-pane fade" id="tab2primary">
                        	<div class="col-md-12 top-10">
                                <ul class="top-10">
                                	<li>Aadhaar Card</li>
                                    <li>Electricity Bill</li>
                                    <li>Telephone Bill</li>
                                    <li>Water Bill</li>
                                    <li>Spouse Passport</li>
                                    <li>Parents Passport</li>
                                    <li>Rent Agreement</li>
                                    <li>Bank Account Passbook</li>
                                    <li>Gas Connection Bill</li>
                                    <li>IT Assessment Order</li>
                                    <li>Employer Certificate</li>
                                </ul>
                                <br>
                            </div>
                        </div><!-- end tab2primary>
                        <div class="tab-pane fade" id="tab3primary">
                        	<div class="col-md-12 top-10">
                                <ul class="top-10">
                                	<li>Pan Card</li>
                                    <li>Aadhar Card</li>
                                    <li>Driving Licence</li>
                                    <li>Voter ID</li>
                                    <li>Birth Certificate</li>
                                    <li>Transfer/School Leaving Certificate</li>
                                    <li>Matriculation/10th/12th Certificate</li>
                                    <li>Service Record/Pay Pension Order</li>
                                    <li>Policy Bond</li>
                                    <li>Orphan Declaration</li>
                                </ul>
                                <br>
                            </div>
                        </div><!--end tab3primary >
                        <div class="tab-pane fade" id="tab4primary">
                        	<div class="col-md-12 top-10">
                                <ul class="top-10">
                                	<li>Matriculation Certificate</li>
									<li>10th Certificate</li>
									<li>12th Certificate</li>
									<li>Higher Educational Pass Certificate</li>
                                </ul>
                               
                            </div>
                        </div><!--end tab4primary >
                        
                    </div>
                </div>
            </div>
        	</div>
            <div class="clb"></div>
       
       <div class="clb"></div-->


				
				</div>
			</div>
		</div>
	</div>
</section>   
		 

@include('includes.footer')



  <style>
  a{
  padding-left: 10px;
  }
  
  </style>
  
  
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

<script src="js/form_validate.js"></script>




</body>

<!-- Mirrored from passportonlineindia.org/documents.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:43 GMT -->
</html>
