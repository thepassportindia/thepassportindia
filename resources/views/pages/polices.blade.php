<!DOCTYPE html>
<html lang="en">

@include('includes.head')

   
<script type="text/javascript">
$(document).ready(function(){


    $('#contact_2_state').on('change',function(){		
        var stateID = $(this).val();
		
		//alert (qualificationID);
		
        if(stateID){
            $.ajax({
                type:'POST',
                url:'locationData.php',		
                
                data:'state_id='+stateID,
                success:function(html){
                    $('#contact_2_district').html(html);
                    
                }
            }); 
        }else{
            $('#contact_2_district').html('<option value="">Select state first</option>');
           
        }
    });
 
});

//End script

</script>
  <style>
  h1{
       text-align:center; 
       color: #000;"
      
  }
  h5 {
    margin-top: 30px;
    text-align: center;
  }
  
  marquee{
     background-color: #5DA6D3;
     font-size: 16px;
     padding-top:5px;
     padding-bottom:5px;
     color: #000; 
     font-weight: 700;
     letter-spacing: 3px;
  }
  .img-11{
     display:none;
 }
 .img-21{
     display:none;
 }
 .img-2>h3{
	 text-align:center;
	 font-size:30px;
	 margin-bottom: -20px;
 }
 
   .section-head{
       background-color: #5DA6D3;
       width:100%; 
       #border-radius: 10px;
       margin-bottom: 15px;
   }
   
  .card{ 
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    #border: 2px solid #686868;
    #border-radius: 15px;
	
	    -webkit-box-shadow: 10px 10px 15px 2px #686868;
	}
	h2{
	color: #1dc8cd;
	}
	h4{
	    color:white;
	}
	.enq{
	    padding-left: 80px;
	    color: #ffffff;
	    font-size:20px;
	}
	.enq-bg{
	#background: #ff8923;
	background: #2e5fa7;
    padding: 10px 10px 10px 10px;
}
.marq-blink {
    #animation:blinkingText 1s infinite;
	}
	
@keyframes blinkingText{
    0%{     color: #000;    }
    #49%{    color: transparent; }
    #50%{    color: transparent; }
    #99%{    color:transparent;  }
    100%{   color: #f28727;    }
}
p{
    color: black;
}
.error{
color: red;
}

	.navbar-default{
	margin-top:140px;
	
	}
	
	.top-logo{
	background-color:#ffffff;
	#margin-left:20px;
	padding-left:30px;
	margin-right:-10px;
	background:cover;
	}
	
	
  .faq-head{
  background: gray;
  padding-top:10px;
  padding-bottom: 10px;
  }
  .btn-warning{
      background: #3e70cb;
  }

 @media only screen and (max-width: 600px) {
	 
	
   .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 10px;
	padding-top: 5px;
	padding-bottom: 5px;
	}
   .main-header{   
     min-height: 110px;
	}
 	
  .img-1{
     display:none;
     height:10px;
     width:10px;
  }
  .img-2{
	  display:none;
      #font:10px;
      #text-align:right;
      #padding-left:10px;
     
   }
   .img-21{
      display:block;
	}
	
 .img-21>h3{
       font-size:16px;
       text-align:left;
       
   }
   
   .img-2 h3{
       font-size: 20px;
       text-align:right;
       margin-top:10px;
    }
   
  .img-3{
     
      display:none;
  }
  .img-11{
     display: none;
     #height:15px;
     #width:15px;
     #margin-left:5px;
  }
  .top-nav-collapse {

	  #padding: 25px 0;
	  }
	.navbar-default {
		margin-top: 0px;
	}
	.form-control {
		margin-top:10px;
	}
	h1{
		font-size:25px;
	}

}


</style>


<style>
    .error{
			display: none;
		
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		input.invalid, textarea.invalid{
		    
		    border:2px solid red;
		}
		
			input.valid, textarea.valid{
		    
		    border:2px solid green;
		}
</style>

</head>
<body>
  <!--header-->
  
  @include('includes.header1')
  <!--/ header-->


  <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>
	<!--div class="row enq-bg">
        	
            <div class="col-md-2 enq-title top-10">
            	<label class="enq">Enquiry</label>
            </div>
            
            <div class="col-md-10 unic">
                
                <form  role="form" method="post" action="enquiry_db.php">
                    
            	<div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Name" name="name" id="txt_top_name" maxlength="100" autocomplete="off" required pattern= "/^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*/" title= "Minimum 1 digit,number not allow"></div>
                
                <div class="col-md-3 top-10"><input type="email" class="form-control center-block" placeholder="Email" name="email" id="txt_top_email" maxlength="100" required></div>
                
                <div class="col-md-3 top-10"><input type="text" class="form-control center-block" placeholder="Phone" name="phone" id="txt_top_phone" maxlength="10" pattern="[6789][0-9]{9}" title="Phone number with 6-7-8-9 and remaing 9 digit with 0-9" required></div>
                <div class="col-md-3 top-10"><button type="submit" name="submit" class="btn btn-warning form-control">Submit</button></div>
                </form>
            </div>
    </div-->
    <!---->
  <section class="section-padding wow fadeInUp delay-05s" id="contact">
    <div class="container card">
      <div class="row white">
        
		
        <div class="col-md-12 col-sm-12">
						<div class="row">
						    
						 <h1 style="text-align:center; color: #ff6613;">Privacy Policies</h1>
                            <hr size="4px">
   
					
		<p class="pse" style="padding-left:40px; padding-right:20px;">This Privacy Policy governs the manner in which thepassportindia.org
		collects, uses, maintains and discloses information collected from users (each, a "User") of 
		the http://www.thepassportindia.org website ("Site"). This privacy policy applies
		to the Site and all the information provided on site.</p>
		


<h4 style="color: #ff6613; text-align:left; padding-left:20px;">1. Personal identification information</h4>
    <p class="pse" style="padding-left:40px; padding-right:20px;">We may collect personal identification information from Users in a variety of ways, 
    including, but not limited to, when Users visit our site, register on the site, place an 
    application, and in connection with other activities, services, features or resources we 
    make available on our Site. Users may be asked for, as appropriate, name, email address, 
    mailing address, phone number. Users may, however, visit our Site anonymously. We will 
    collect personal identification information from Users only if they voluntarily submit 
    such information to us. Users can always refuse to supply personal identification information,
    except that it may prevent them from engaging in certain Site related activities.</p>

  <h4 style="color: #ff6613; text-align:left; padding-left:20px;">2. Non-personal identification information</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">We may collect non-personal identification information about Users whenever
                they interact with our Site. Non-personal identification information may include 
                the browser name, the type of computer and technical information about Users means 
                of connection to our Site, such as the operating system and the Internet service providers' 
                utilized and other similar information.</p>
                
  <h4 style="color: #ff6613; text-align:left; padding-left:20px;">3. Web browser cookies</h4>
                 <p class="pse" style="padding-left:40px; padding-right:20px;">Our Site may use "cookies" to enhance User experience. User's web browser places cookies 
                 on their hard drive for record-keeping purposes and sometimes to track information about them.
                 User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent.
                 If they do so, note that some parts of the Site may not function properly.</p>
                 
   <h4 style="color: #ff6613; text-align:left; padding-left:20px;">4. How we protect your information</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">We adopt appropriate data collection, storage and processing practices and security measures to protect against 
                unauthorized access, alteration, disclosure or destruction of your personal information, transaction information and
                data stored on our Site.</p>
                
                <p class="pse" style="padding-left:40px; padding-right:20px;">Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel
                and is encrypted and protected with digital signatures. Our Site is also in compliance with PCI vulnerability standards
                in order to create as secure of an environment as possible for Users.</p>
                
    <h4 style="color: #ff6613; text-align:left; padding-left:20px;">5. Compliance with children's online privacy protection act</h4>
 
                <p class="pse" style="padding-left:40px; padding-right:20px;">Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain 
                information at our Site from those we actually know are under 13, and no part of our website is structured to attract 
                anyone under 13.</p>
                
    <h4 style="color: #ff6613; text-align:left; padding-left:20px;">6.  Changes to this privacy policy</h4>
                <p class="pse" style="padding-left:40px; padding-right:20px;">passportonlineregistration.org have the discretion to update this privacy policy at any time. We encourage Users to 
                frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. 
                You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
                

	
				</div>
			</div>
		</div>
	</div>
    </section>   
		 


  
 @include('includes.footer')
  <!---->
  <!--contact ends-->
  <style>
  a{
  padding-left: 10px;
  }
  
  </style>
  
  
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

<script src="js/form_validate.js"></script>




</body>

<!-- Mirrored from passportonlineindia.org/policies.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:46 GMT -->
</html>
