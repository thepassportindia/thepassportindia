<!DOCTYPE html>
<html lang="en">

@include('includes.head')

</script>
<style>
    h1 {
        text-align: center;
        color: #000;
        "

    }
    
    h5 {
        margin-top: 30px;
        text-align: center;
    }
    
    marquee {
        background-color: #5DA6D3;
        font-size: 16px;
        padding-top: 5px;
        padding-bottom: 5px;
        color: #000;
        font-weight: 700;
        letter-spacing: 3px;
    }
    
    .img-11 {
        display: none;
    }
    
    .img-21 {
        display: none;
    }
    
    .img-2>h3 {
        text-align: center;
        font-size: 30px;
        margin-bottom: -20px;
    }
    
    .section-head {
        background-color: #5DA6D3;
        width: 100%;
        #border-radius: 10px;
        margin-bottom: 15px;
    }
    
    .card {
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        #border: 2px solid #686868;
        #border-radius: 15px;
        -webkit-box-shadow: 10px 10px 15px 2px #686868;
    }
    
    h2 {
        color: #1dc8cd;
    }
    
    h4 {
        color: white;
    }
    
    .enq {
        padding-left: 80px;
        color: #ffffff;
        font-size: 20px;
    }
    
    .enq-bg {
        #background: #ff8923;
        background: #2e5fa7;
        padding: 10px 10px 10px 10px;
    }
    
    .marq-blink {
        #animation: blinkingText 1s infinite;
    }
    
    @keyframes blinkingText {
        0% {
            color: #000;
        }
        #49% {
            color: transparent;
        }
        #50% {
            color: transparent;
        }
        #99% {
            color: transparent;
        }
        100% {
            color: #f28727;
        }
    }
    
    p {
        color: black;
    }
    
    .error {
        color: red;
    }
    
    .navbar-default {
        margin-top: 140px;
    }
    
    .top-logo {
        background-color: #ffffff;
        #margin-left: 20px;
        padding-left: 30px;
        margin-right: -10px;
        background: cover;
    }
    
    .faq-head {
        background: gray;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    
    .btn-warning {
        background: #3e70cb;
    }
    
    @media only screen and (max-width: 600px) {
        .h4,
        .h5,
        .h6,
        h4,
        h5,
        h6 {
            margin-top: 0px;
            margin-bottom: 10px;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .main-header {
            min-height: 110px;
        }
        .img-1 {
            display: none;
            height: 10px;
            width: 10px;
        }
        .img-2 {
            display: none;
            #font: 10px;
            #text-align: right;
            #padding-left: 10px;
        }
        .img-21 {
            display: block;
        }
        .img-21>h3 {
            font-size: 16px;
            text-align: left;
        }
        .img-2 h3 {
            font-size: 20px;
            text-align: right;
            margin-top: 10px;
        }
        .img-3 {
            display: none;
        }
        .img-11 {
            display: none;
            #height: 15px;
            #width: 15px;
            #margin-left: 5px;
        }
        .top-nav-collapse {
            #padding: 25px 0;
        }
        .navbar-default {
            margin-top: 0px;
        }
        .form-control {
            margin-top: 10px;
        }
        h1 {
            font-size: 25px;
        }
    }
</style>

<style>
    .error {
        display: none;
    }
    
    .error_show {
        color: red;
        margin-left: 10px;
    }
    
    input.invalid,
    textarea.invalid {
        border: 2px solid red;
    }
    
    input.valid,
    textarea.valid {
        border: 2px solid green;
    }
</style>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145237464-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-145237464-1');
</script>

<script>
    < meta name = "google-site-verification"
    content = "ix1c5DUJGo0ZhWQtxvPcdi0xsIABYjqX0xtjLIUcDrI" / >
</script>
</head>

<body>

    @include('includes.header1')

    <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>

    <h1>Fees Details</h1>
    <hr size="4px">
    <section class="section-padding" id="contact">
        <div class="container card">
            <div class="row white">

                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <h1>New/ Fresh passport Application</h1>
                        <hr size="4px">

                        <div class="table-responsive">
                            <table class="table">
                                <thead style="font-size:15px; font-weight:500;">
                                    <tr>
                                        <th>Age of Applicant</th>
                                        <th>Fees for Normal Application</th>
                                        <th>Additional Fees for Tatkal Application</th>
                                        <th>Fees Upto 36 Pages</th>
                                        <th>Additional Fees for Extra Pages (36+24=60)</th>
                                        <th>Consultancy Fees</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td>
                                            Below 18

                                        </td>
                                        <td>
                                            1000
                                        </td>
                                        <td>
                                            2000
                                        </td>
                                        <td>No Fees </td>
                                        <td> Not Applicable</td>
                                        <td> 1000</td>
                                    </tr>

                                    <tr>

                                        <td>
                                            Above 18

                                        </td>
                                        <td>
                                            1500
                                        </td>
                                        <td>
                                            2000
                                        </td>
                                        <td>No Fees </td>
                                        <td> 500</td>
                                        <td> 1000</td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-padding" id="contact">
        <div class="container card">
            <div class="row white">

                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <h1>Reissue passport Application</h1>
                        <hr size="4px">

                        <div class="table-responsive">
                            <table class="table">
                                <thead style="font-size:15px; font-weight:500;">
                                    <tr>
                                        <th>Age of Applicant</th>
                                        <th>Fees for Normal Application</th>
                                        <th>Additional Fees for Tatkal Application</th>
                                        <th>Fees Upto 36 Pages</th>
                                        <th>Additional Fees for Extra Pages (36+24=60)</th>
                                        <th>Lost / Damaged but Not Expired</th>
                                        <th>Consultancy Fees</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Below 18

                                        </td>
                                        <td>
                                            1000
                                        </td>
                                        <td>
                                            2000
                                        </td>
                                        <td>No Fees </td>
                                        <td> Not Applicable</td>
                                        <td> 1500</td>
                                        <td> 1000</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Above 18
                                        </td>
                                        <td>
                                            1500
                                        </td>
                                        <td>
                                            2000
                                        </td>
                                        <td>No Fees </td>
                                        <td> 500</td>
                                        <td> 1500</td>
                                        <td> 1000</td>
                                    </tr>

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('includes.footer')

    <style>
        a {
            padding-left: 10px;
        }
    </style>

    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/custom.js"></script>
    <script src="contactform/contactform.js"></script>

    <script src="js/form_validate.js"></script>

</body>

<!-- Mirrored from passportonlineindia.org/fee.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:43 GMT -->

</html>