<!DOCTYPE html>
<html lang="en">

@include('includes.head')

  <style>
  h1{
       text-align:center; 
       color: #000;"
      
  }
  h5 {
    margin-top: 30px;
    text-align: center;
  }
  
  marquee{
     background-color: #5DA6D3;
     font-size: 16px;
     padding-top:5px;
     padding-bottom:5px;
     color: #000; 
     font-weight: 700;
     letter-spacing: 3px;
  }
  .img-11{
     display:none;
 }
 .img-21{
     display:none;
 }
 .img-2>h3{
	 text-align:center;
	 font-size:30px;
	 margin-bottom: -20px;
 }
 
   .section-head{
       background-color: #5DA6D3;
       width:100%; 
       #border-radius: 10px;
       margin-bottom: 15px;
   }
   
  .card{ 
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    #border: 2px solid #686868;
    #border-radius: 15px;
	
	    -webkit-box-shadow: 10px 10px 15px 2px #686868;
	}
	h2{
	color: #1dc8cd;
	}
	h4{
	    color:white;
	}
	.enq{
	    padding-left: 80px;
	    color: #ffffff;
	    font-size:20px;
	}
	.enq-bg{
	#background: #ff8923;
	background: #2e5fa7;
    padding: 10px 10px 10px 10px;
}
.marq-blink {
    #animation:blinkingText 1s infinite;
	}
	
@keyframes blinkingText{
    0%{     color: #000;    }
    #49%{    color: transparent; }
    #50%{    color: transparent; }
    #99%{    color:transparent;  }
    100%{   color: #f28727;    }
}
p{
    color: black;
}
.error{
color: red;
}

	.navbar-default{
	margin-top:140px;
	
	}
	
	.top-logo{
	background-color:#ffffff;
	#margin-left:20px;
	padding-left:30px;
	margin-right:-10px;
	background:cover;
	}
	
	
  .faq-head{
  background: gray;
  padding-top:10px;
  padding-bottom: 10px;
  }
  .btn-warning{
      background: #3e70cb;
  }

 @media only screen and (max-width: 600px) {
	 
	
   .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 10px;
	padding-top: 5px;
	padding-bottom: 5px;
	}
   .main-header{   
     min-height: 110px;
	}
 	
  .img-1{
     display:none;
     height:10px;
     width:10px;
  }
  .img-2{
	  display:none;
      #font:10px;
      #text-align:right;
      #padding-left:10px;
     
   }
   .img-21{
      display:block;
	}
	
 .img-21>h3{
       font-size:16px;
       text-align:left;
       
   }
   
   .img-2 h3{
       font-size: 20px;
       text-align:right;
       margin-top:10px;
    }
   
  .img-3{
     
      display:none;
  }
  .img-11{
     display: none;
     #height:15px;
     #width:15px;
     #margin-left:5px;
  }
  .top-nav-collapse {

	  #padding: 25px 0;
	  }
	.navbar-default {
		margin-top: 0px;
	}
	.form-control {
		margin-top:10px;
	}
	h1{
		font-size:25px;
	}

}


</style>


<style>
    .error{
			display: none;
		
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		input.invalid, textarea.invalid{
		    
		    border:2px solid red;
		}
		
			input.valid, textarea.valid{
		    
		    border:2px solid green;
		}
</style>
</head>

<body>
 
 @include('includes.header1')
  <marquee scrollamount="15"> This site is owned by a Private Organization & Not Associated with Ministry of External Affairs (MEA)</marquee>

  <section class="section-padding" id="contact">
    <div class="container card">
      <div class="row white">
        
		
        <div class="col-md-12 col-sm-12">
						<div class="row">
						 <h1>Frequently Ask Question</h1>
						<hr size="4px">
  
   
		<div class="col-md-6">
 		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What is Ordinary, Diplomatic or Official Passport ?</a>
          <div class="faqanswer">
            <p> All private citizens applies for an ordinary passport however the other two types of passports are for government workers who are being send overseas on official business only.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What Is Passport ?</a>
          <div class="faqanswer">
            <p> A passport is a travel document issued by a country’s government to its citizens that verifies the identity and nationality of the holder for the purpose of international travel.
Passports are small booklets that typically contain the bearer’s name, place of birth, date of birth, the date of issue, date of expiry, passport number, photo and signature.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> Who qualifies for an Indian passport ?</a>
          <div class="faqanswer">
            <p> You can qualify for Indian citizenship by being born in the country, being born elsewhere but with at least one Indian parent, or by being granted citizenship through a naturalization process. If none of these categories apply you will not be classed as an Indian citizen, and will not be entitled to an Indian passport.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What happens if I lose my passport ?</a>
          <div class="faqanswer">
            <p> Passports are important legal documents and if you lose your passport or suspect it might have been stolen, this has to be reported to the Police. You can then apply to have your passport reissued.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> How many days it will take to get Passport ?</a>
          <div class="faqanswer">
            <p> As per Govt. the standard timeline is 30 days from the date of application.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What are the documents required for Fresh passport ?</a>
          <div class="faqanswer">
            <p> Fresh Application: Click on the below link http://passportonlineindia.org/document.php</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What are the documents required for Re-Issue Passport ?</a>
          <div class="faqanswer">
            <p> Click on the below link http://passportonlineindia.org/document.php</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What is Tatkaal Passport ?</a>
          <div class="faqanswer">
            <p> This is the service, allowing you to obtain your passport in a shorter time. It costs twice as much as the standard passport service but is a good option for people who require their passport in a hurry.</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> What are the documents required for Tatkal passport?</a>
          <div class="faqanswer">
            <p> http://passportonlineindia.org/document.php</p>
   </div>
   
   		    	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i> How much time it will take to get Tatkaal Passport ?</a>
          <div class="faqanswer">
            <p> Tatkaal Passport takes 3-4 working days excluding date of submission and without Police Verification.</p>
   </div>
   
   
	
	</div>	
		
		
 		
		<!--Right Side-->					
	<div class="col-md-6">	
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>What are the documents required for Lost / Damaged Passport ?</a>
            <div class="faqanswer">
                <p>Affidavit with details of how a passport got damaged or was lost (Annexure L)

No Objection Certificate (Annexure M) / Prior Intimation Letter (Annexure N)

Current address (proof) / Address Proof / Identity Proof

Police report (FIR)

Semi-literate or literate applicants: Affidavit sworn before a notary stating the place and date of birth (Annexure A)

Photocopy of the first and last pages of old passport (ECR/Non-ECR page), if available (optional)

Passport-size photographs</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>What happens if you want to Cancel/Reschedule a Visit to Passport Seva Kendra ?</a>
            <div class="faqanswer">
                <p>Applicants with confirmed appointments, due to any reason/reasons if you are not able to make the visit on appointment date, you can cancel or reschedule the appointment for 2 times with in that year of first appointment date.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>What is a PSK ?</a>
            <div class="faqanswer">
                <p>PSK - Passport Service Kendra - is the passport office where you can get information about your passport and apply for either a new passport or renewal of an existing passport. There are dozens of different PSK offices across India.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>I'm living outside India, do I have to travel back to get my passport ?</a>
            <div class="faqanswer">
                <p>If you have lost your passport while overseas, or have moved overseas permanently but want to renew an Indian passport, then Indian Embassies, Consulates and High Commissions across the globe can perform the same services as the passport offices in India.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>Are the PSK offices open at the weekend ?</a>
            <div class="faqanswer">
                <p>No, the PSK core hours are between 9.30am and 4.30pm, Monday to Friday. They are not open at the weekend, even for urgent issues. Make an appointment online for a time which is most convenient for you.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>How long do I have to book my appointment after filling in the form ?</a>
            <div class="faqanswer">
                <p>It's wise to book your appointment for as soon as you can after filling in your application online. System allows you to select the appointment time which is most suitable, try not to let things slip too long.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>Can I change details on my passport ?</a>
            <div class="faqanswer">
                <p>If you've changed your name because you've got married, or other details on your passport are incorrect, then you can apply to have these corrected.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>What languages are passports issued in ?</a>
            <div class="faqanswer">
                <p>Indian passports have two languages on them, English and Hindi. There is no option to choose to have passports issued in other languages.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>Who can help if I have questions about my passport?</a>
            <div class="faqanswer">
                <p>You can reach to us for any questions related to your Passport.</p>
            </div>
   
	
	
			    <a href="#" class="togglefaq"><i class="glyphicon-plus"></i>Can someone else attend my passport appointment for me ?</a>
            <div class="faqanswer">
                <p>You have to attend in person for your passport interview, even if you are ill or if it is very inconvenient for you to do so. You are allowed to bring someone with you for your passport interview if you find it difficult to travel.</p>
            </div>
   
   
     
	</div>						
								
				</div>
			</div>
		</div>
	</div>
    </section>   
	<style>
	    /* FAQ COLLAPSE/EXPAND STYLES */
* {
  box-sizing: border-box;
}
.faqanswer {
	display: none;
	#width: 590px;
	background: #ffffff;
	padding: 12px 20px 0 30px;	
	border: 1px solid #000000;
}

.faqanswer p {
	font-size: 13px;
	line-height: 17px;	
}


a.active {
	font-weight: bold;
	color: #ff6613;
}

.togglefaq {
    background: #f4f4f4;
	text-decoration: none;
	color: #333;
	font-size: 13px;
	padding: 10px 30px;
	line-height: 20px;
	display: block;
	#border: 1px solid #000000;
	border-radius 25px;
	margin-bottom: -1px;
	font-weight: bold;
	margin-top: 5px;
	margin-bottom: 5px;
}
.glyphicon-plus {
	color: #ff6613;
	margin-right: 20px;
	font-size: 30px;
	float:right;
}

.glyphicon-minus {
	color: #ff6613;
	margin-right: 20px;
	font-size: 30px;
	float:right;
}
p {
  margin: 0;
  padding-bottom: 20px;
}

	</style>	 
<script>
    
    //faq toggle stuff 
$('.togglefaq').click(function(e) {
e.preventDefault();
var notthis = $('.active').not(this);
notthis.find('.glyphicon-minus').addClass('glyphicon-plus').removeClass('glyphicon-minus');
notthis.toggleClass('active').next('.faqanswer').slideToggle(300);
 $(this).toggleClass('active').next().slideToggle("fast");
$(this).children('i').toggleClass('glyphicon-plus glyphicon-minus');
});
</script>


@include('includes.footer')

  <style>
  a{
  padding-left: 10px;
  }
  
  </style>
  
  
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/wow.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>

<script src="js/form_validate.js"></script>




</body>

<!-- Mirrored from passportonlineindia.org/faq.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 03 Jul 2019 16:46:43 GMT -->
</html>
